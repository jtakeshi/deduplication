from __future__ import print_function
#from scipy.sparse import csr_matrix
import getopt
import sys
import time
from LLSH import LLSH
import pickle
import os

from list_features import get_model
from list_features import image_features

from generate_distortions import distort
from generate_distortions import get_hardcoded_params

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3  
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  #Use this to switch between dbm and shelve
  storage_type = 'dbm'

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  
  distortion_type = ''
  num_to_distort = 5
  overwrite = False

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:d:n:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True   
    elif opt in ('-d'):
      distortion_type = arg     
    elif opt in ('-n'):
      num_to_distort = int(arg)  
    elif opt in ('-o'):
      overwrite = True  


  #Check inputs
  bad_input = False
  if matrix_filename == '':
    print("No matrices file given!")
    bad_input = True
  if shelvefile == '':  
    print("No database given!")
    bad_input = True    

  if bad_input:
    return    

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes

  base_dir = remainders[0]
  if base_dir[-1] != '/':
    base_dir += '/'
  first_level_subdirs = [base_dir + x + '/' for x in os.listdir(base_dir) if os.path.isdir(base_dir + x + '/')]
  #DEBUG
  #print(first_level_subdirs)
  images_to_distort = list()
  images_to_index = list()
  for subdir in first_level_subdirs:
    second_level_files = [x for x in os.listdir(subdir) if x[-4:] == '.jpg']
    to_distort = second_level_files[:num_to_distort]
    to_index = second_level_files[num_to_distort:2*num_to_distort]
    #DEBUG
    #print(second_level_files)
    for s in to_distort:
      fullpath = subdir + s
      images_to_distort.append(fullpath)
      images_to_index.append(fullpath)
    if overwrite:  
      for s in to_index:
        fullpath = subdir + s
        images_to_index.append(fullpath)  
      
      
  storage_config = dict()
  storage_config[storage_type] = shelvefile   
  
  hashObj = None
  model = None
  if overwrite: 
    hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=overwrite, private_config=privacy_config)
    model = get_model()    
  
  dist_params = get_hardcoded_params()[distortion_type]
      
      
  #Index, distort, and print list of files    
  for filename in images_to_distort:
    if overwrite:
      features = image_features(filename, model)
      hashObj.index(features, filename)
    outline = filename + ' ' + distortion_type
    for p in dist_params:
      outfile = distort(img, distortion_type, p) 
      parm_str = ''
      if distortion_type == 'gaussian':
        parm_str = p['sigma']
      elif distortion_type == 'sp':
        parm_str = p['amount']
      elif distortion_type == 'poisson':
        parm_str = ''   
      else:
        parm_str = p
      outline += ' ' + parm_str + ':' + outfile
    print(outline)  
    #print(filename)
    
  if overwrite:  
    for filename in images_to_index:
      features = image_features(filename, model)
      hashObj.index(features, filename)
    

  hashObj.close()
   
  

if __name__ == "__main__":
  main()  
