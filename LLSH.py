# LLSH - Light Locality-Sensitive Hashing
# Modified from the original LSH: https://github.com/kayzhu/LSHash

import os
import json
import numpy as np
import hashlib
import sys
import threading
import time

from LLSH_storage import storage

try:
    from bitarray import bitarray
except ImportError:
    bitarray = None


class LLSH(object):
    """ LSHash implments locality sensitive hashing using random projection for
    input vectors of dimension `input_dim`.

    Attributes:

    :param hash_size:
        The length of the resulting binary hash in integer. E.g., 32 means the
        resulting binary hash will be 32-bit long.
    :param input_dim:
        The dimension of the input vector. E.g., a grey-scale picture of 30x30
        pixels will have an input dimension of 900.
    :param num_hashtables:
        (optional) The number of hash tables used for multiple lookups.
    :param storage_config:
        (optional) A dictionary of the form `{backend_name: config}` where
        `backend_name` is the either `dict` or `redis`, and `config` is the
        configuration used by the backend. For `redis` it should be in the
        format of `{"redis": {"host": hostname, "port": port_num}}`, where
        `hostname` is normally `localhost` and `port` is normally 6379.
    :param matrices_filename:
        (optional) Specify the path to the compressed numpy file ending with
        extension `.npz`, where the uniform random planes are stored, or to be
        stored if the file does not exist yet.
    :param overwrite:
        (optional) Whether to overwrite the matrices file if it already exist
    :param private_config:
        (optional) Specifies parameters for PLSH. A dictionary data indicating the salt and number of bytes to use. 
        Maybe update later with an algorithm? SHA256 is hardcoded for now.
    """

    def __init__(self, hash_size, input_dim, num_hashtables=1,
                 storage_config=None, matrices_filename=None, 
                 overwrite=False, private_config=None, multithreaded=False):

        self.hash_size = hash_size
        self.input_dim = input_dim
        self.num_hashtables = num_hashtables

        if storage_config is None:
            storage_config = {'dict': None}
        self.storage_config = storage_config

        if matrices_filename and not matrices_filename.endswith('.npz'):
            raise ValueError("The specified file name must end with .npz")
        self.matrices_filename = matrices_filename
        self.overwrite = overwrite

        self._init_uniform_planes()
        self._init_hashtables()
        
        #Error checking - check we have the same number of tables and matrices
        if len(self.hash_tables) != len(self.uniform_planes):
          print("ERROR: sizes of storage and parameters differ")
          print("Hash tables: " + str(len(self.hash_tables)))
          print("Matrices: " + str(len(self.uniform_planes)))

        #Check the PLSH config
        if private_config is not None:
            self.salt = private_config.get('salt', None)
            testobj = hashlib.sha256()
            max_bytes = testobj.digest_size
            self.PLSH_bytes = private_config.get('bytes', max_bytes)
            if self.PLSH_bytes > max_bytes or self.PLSH_bytes < 1:
                raise ValueError("Number of bytes to use must be between 1 and " + str(max_bytes))
            self.PLSH = True
        else:
            self.PLSH = False
            self.salt = None
            self.PLSH_bytes = 0            

        self.count = 0
        self.lock = None
        if multithreaded:
          self.lock = threading.Lock()

    def _init_uniform_planes(self):
        """ Initialize uniform planes used to calculate the hashes

        if file `self.matrices_filename` exist and `self.overwrite` is
        selected, save the uniform planes to the specified file.

        if file `self.matrices_filename` exist and `self.overwrite` is not
        selected, load the matrix with `np.load`.

        if file `self.matrices_filename` does not exist and regardless of
        `self.overwrite`, only set `self.uniform_planes`.
        """

        if "uniform_planes" in self.__dict__:
            return

        if self.matrices_filename:
            file_exist = os.path.isfile(self.matrices_filename)
            if file_exist and not self.overwrite:
                try:
                    npzfiles = np.load(self.matrices_filename)
                except IOError:
                    print("Cannot load specified file as a numpy array")
                    raise
                else:
                    npzfiles = sorted(npzfiles.items(), key=lambda x: x[0])
                    self.uniform_planes = [t[1] for t in npzfiles]
            else:
                self.uniform_planes = [self._generate_uniform_planes()
                                       for _ in xrange(self.num_hashtables)]
                try:
                    np.savez_compressed(self.matrices_filename,
                                        *self.uniform_planes)
                except IOError:
                    print("IOError when saving matrices to specificed path")
                    raise
        else:
            self.uniform_planes = [self._generate_uniform_planes()
                                   for _ in xrange(self.num_hashtables)]

    def _init_hashtables(self):
        """ Initialize the hash tables such that each record will be in the
        form of "[storage1, storage2, ...]" """

        self.hash_tables = [storage(self.storage_config, i)
                            for i in xrange(self.num_hashtables)]

    def _generate_uniform_planes(self):
        """ Generate uniformly distributed hyperplanes and return it as a 2D
        numpy array.
        """

        return np.random.randn(self.hash_size, self.input_dim)
        
    def hash_test(self, input_point):
        for i, table in enumerate(self.hash_tables):
          binary_hash = self._hash(self.uniform_planes[i], input_point)
          if self.PLSH:
            chash = hashlib.sha256()
            if self.salt is not None:
              chash.update(self.salt)
            chash.update(binary_hash)
            result = chash.digest() 
                 

    def _hash(self, planes, input_point):
        """ Generates the binary hash for `input_point` and returns it.

        :param planes:
            The planes are random uniform planes with a dimension of
            `hash_size` * `input_dim`.
        :param input_point:
            A Python tuple or list object that contains only numbers.
            The dimension needs to be 1 * `input_dim`.
        """

        try:
            input_point = np.array(input_point)  # for faster dot product
            projections = np.dot(planes, input_point)
        except TypeError as e:
            print("""The input point needs to be an array-like object with
                  numbers only elements""")
            raise
        except ValueError as e:
            print("""The input point needs to be of the same dimension as
                  `input_dim` when initializing this LSHash instance""", e)
            raise
        else:
            lsh_result = "".join(['1' if i > 0 else '0' for i in projections])
            #If no privacy, simply return the LSH result. If privacy, run the hash on the LSH result.
            if not self.PLSH:
                return lsh_result
            else:
                chash = hashlib.sha256()
                if self.salt is not None:
                    chash.update(self.salt)
                chash.update(lsh_result)
                return chash.digest()[:self.PLSH_bytes]

    def _as_np_array(self, json_or_tuple):
        """ Takes either a JSON-serialized data structure or a tuple that has
        the original input points stored, and returns the original input point
        in numpy array format.
        """
        if isinstance(json_or_tuple, basestring):
            # JSON-serialized in the case of Redis
            try:
                # Return the point stored as list, without the extra data
                tuples = json.loads(json_or_tuple)[0]
            except TypeError:
                print("The value stored is not JSON-serilizable")
                raise
        else:
            # If extra_data exists, `tuples` is the entire
            # (point:tuple, extra_data). Otherwise (i.e., extra_data=None),
            # return the point stored as a tuple
            tuples = json_or_tuple

        if isinstance(tuples[0], tuple):
            # in this case extra data exists
            return np.asarray(tuples[0])

        elif isinstance(tuples, (tuple, list)):
            try:
                return np.asarray(tuples)
            except ValueError as e:
                print("The input needs to be an array-like object", e)
                raise
        else:
            raise TypeError("query data is not supported")

    def index(self, input_point, extra_data=None):
        """ Index a single input point by adding it to the selected storage.

        If `extra_data` is provided, it will become the value of the dictionary
        {input_point: extra_data}, which in turn will become the value of the
        hash table. `extra_data` needs to be JSON serializable if in-memory
        dict is not used as storage.

        :param input_point:
            A list, or tuple, or numpy ndarray object that contains numbers
            only. The dimension needs to be 1 * `input_dim`.
            This object will be converted to Python tuple and stored in the
            selected storage.
        :param extra_data:
            (optional) Needs to be a JSON-serializable object: list, dicts and
            basic types such as strings and integers.
        """

        """
        if isinstance(input_point, np.ndarray):
            input_point = input_point.tolist()

        if extra_data:
            value = (tuple(input_point), extra_data)
        else:
            value = tuple(input_point)
        """

        if extra_data is None:
          raise ValueError("extra_data is not optional")
        #Store only the "extra data" (filename)
        for i, table in enumerate(self.hash_tables):
            table.append_val(self._hash(self.uniform_planes[i], input_point),
                             extra_data)
        self.count = self.count + 1    
        
    #Indexes with hash given. Definitely lock/unlock when using this!    
    def index_nohash_time(self, computed_hash, extra_data=None):
      if extra_data is None:
        extra_data = '0'
        #extra_data would normally be required, but this is a testing function
        #raise ValueError("extra_data is not optional")
        #Store only the "extra data" (filename)
      start = time.time()  
      if self.lock is None:
        for i, table in enumerate(self.hash_tables):
          table.append_val(computed_hash[i], extra_data)
      else:
        with self.lock:  
          for i, table in enumerate(self.hash_tables):
            table.append_val(computed_hash[i], extra_data)
      self.count = self.count + 1    
      end = time.time()    
      return end-start

    def query(self, query_point, num_results=None, debug=False):
        """ Takes `query_point` which is either a tuple or a list of numbers,
        returns `num_results` of results as a list of tuples that are ranked
        based on the supplied metric function `distance_func`.

        :param query_point:
            A list, or tuple, or numpy ndarray that only contains numbers.
            The dimension needs to be 1 * `input_dim`.
            Used by :meth:`._hash`.
        :param num_results:
            (optional) Integer, specifies the max amount of results to be
            returned. If not specified all candidates will be returned as a
            list in ranked order.
        :param distance_func:
            (optional) The distance function to be used. Currently it needs to
            be one of ("hamming", "euclidean", "true_euclidean",
            "centred_euclidean", "cosine", "l1norm"). By default "euclidean"
            will used.
        """

        #Counts candidates by number of collisions

        candidates = dict()
        for i, table in enumerate(self.hash_tables):
            binary_hash = self._hash(self.uniform_planes[i], query_point)
            cand_list = table.get_list(binary_hash)
            if cand_list is None:
              continue
            for filename in cand_list:
                if filename in candidates:
                    candidates[filename] += 1
                else:
                    candidates[filename] = 1   
                #DEBUG
                #print(filename + " collided! " + str(candidates[filename]))    

        #DEBUG
        #print("Candidates: ")
        #print(candidates)

        #TODO add in min. threshold required
        if debug:
            print("Candidates: " + str(candidates))
        #Sort candidates by collisions            
        candidates_sorted = [pair for pair in candidates.items()]
        candidates_sorted.sort(key=lambda x: x[1], reverse=True)
        return candidates_sorted[:num_results] if num_results is not None else candidates_sorted

    #Use query_hashes to get a hash, and query_nohash to test table retrieval time only
    def query_hashes(self, query_point):
      bin_hash = list()
      for i, table in enumerate(self.hash_tables):
        bin_hash.append(self._hash(self.uniform_planes[i], query_point))
      return bin_hash

    def query_nohash(self, computed_hash, num_results=None, debug=False):
      candidates = dict()
      for i, table, in enumerate(self.hash_tables):
          cand_list = table.get_list(computed_hash[i])
          if cand_list is None:
            continue
          for filename in cand_list:
              if filename in candidates:
                  candidates[filename] += 1
              else:
                  candidates[filename] = 1   
              #DEBUG
              #print(filename + " collided! " + str(candidates[filename]))    

      #DEBUG
      #print("Candidates: ")
      #print(candidates)

      #TODO add in min. threshold required
      if debug:
          print("Candidates: " + str(candidates))
      #Sort candidates by collisions            
      candidates_sorted = [pair for pair in candidates.items()]
      candidates_sorted.sort(key=lambda x: x[1], reverse=True)
      return candidates_sorted

    def __str__(self):
        retdicts = [dict() for x in range(len(self.hash_tables))]
        for i, table in enumerate(self.hash_tables):
            for k in table.keys():
                tmplist = table.get_list(k)
                if tmplist:
                    retdicts[i][k] = tmplist
        return "".join([str(t) for t in retdicts])    


    def close(self):
        if 'shelve' in self.storage_config or 'dbm' in self.storage_config:
            for t in self.hash_tables:
                t.close()

    def num_indexed(self):
        return self.count            

    ### distance functions

    @staticmethod
    def hamming_dist(bitarray1, bitarray2):
        xor_result = bitarray(bitarray1) ^ bitarray(bitarray2)
        return xor_result.count()

    @staticmethod
    def euclidean_dist(x, y):
        """ This is a hot function, hence some optimizations are made. """
        diff = np.array(x) - y
        return np.sqrt(np.dot(diff, diff))

    @staticmethod
    def euclidean_dist_square(x, y):
        """ This is a hot function, hence some optimizations are made. """
        diff = np.array(x) - y
        return np.dot(diff, diff)

    @staticmethod
    def euclidean_dist_centred(x, y):
        """ This is a hot function, hence some optimizations are made. """
        diff = np.mean(x) - np.mean(y)
        return np.dot(diff, diff)

    @staticmethod
    def l1norm_dist(x, y):
        return sum(abs(x - y))

    @staticmethod
    def cosine_dist(x, y):
        return 1 - np.dot(x, y) / ((np.dot(x, x) * np.dot(y, y)) ** 0.5)
