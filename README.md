This repository contains several scripts for secure nearly-identical image deduplication, along with a profiling application for PAKE. 

Organization: features, hash_results, images, and queries are folders used to store input/output to scripts. jobs stores scripts for the ND CRC. scrap stores misc. files, including virtualenv package lists, an example of running a deduplication test, and various scripts.

The use of a virtual environment (https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) is highly recommended. 
Dependencies for feature extraction (which is also performed in a query) are keras and tensorflow (either CPU or GPU).
See scrap/pipeline.sh as an example of how to run the 3 tests in succession. A set of images must first have features extracted with list_features.py. Then, an index can be built with build_index. Afterwards, queries for image similarity can be run with query.py.
Arguments for building and querying a hash:
  -h: hash table size - must be large enough so that 2 ** arg easily contains the number of images to be indexed. Started at 16, but could be moved to 32.
  -t: number of hash tables. 4 has been good so far
  -m: matrix filename, highly recommended to have these in their own directory
  -s shelve filename
These parameters must be synchronized between building and querying an index.

add_noise.py is a script to add noise to images, for testing the threshold at which deduplication fails. To run the image noise adder, opencv-python is needed. First, install numpy and matplotlib.
Arguments:
  -n: type of noise. One of "gauss", "s&p", "poisson", "speckle"
  -v: variance for Gaussian noise
  -m: mean for Gaussian noise
  -s: salt/pepper balance for s&p noise
  -a: amount for Gaussian
  -o: output filename
Example: $ python add_noise.py -n gauss -v 300.0 -m 0 images/ggb_original.jpg  
Without -o, the output file will have ".noisy" added as a penultimate extension, e.g. the output for the above example will be in images/ggb_original.noisy.jpg . ASSUMES CONVENTIONAL FILENAMES.

For PAKE testing, compile pake.cpp: g++ pake.cpp -o pake -lgmp -lgmpxx -std=c++11
Arguments:
  -a, -b: Integer arguments for Alice/Bob
  -p: Number of bits to use for the prime
  -v: Verbose output
