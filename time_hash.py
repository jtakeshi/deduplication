from __future__ import print_function
#from scipy.sparse import csr_matrix
import getopt
import sys
import time
from LLSH import LLSH
import pickle


#Returns an array of 2-tuples - first entry is filename, second is feature vector
def read_features(input_filename):
  #print("Reading features from " + input_filename)
  f = open(input_filename,'r')
  lines = f.readlines()
  tuples = []
  for line in lines:
    #vals = line.split(' ')
    if line[0] == '#':
      continue
    split_line = line.split()
    fname = split_line[0]
    numbers = [float(i) for i in split_line[1:]]
    values_tuple = (fname, numbers)
    tuples.append(values_tuple)
  return tuples  


def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3  
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  #Use this to switch between dbm and shelve
  storage_type = 'dbm'
  num_runs = 0

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:n:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True   
    elif opt in ('-n'):
      num_runs = int(arg)     

  
  #Check inputs
  bad_input = False
  if matrix_filename == '':
    print("No matrices file given!")
    bad_input = True
  if shelvefile == '':  
    print("No database given!")
    bad_input = True    

  if bad_input:
    return    

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes

  feature_file = open(remainders[0])
  #print("#Reading features from " + feature_file)    
  storage_config = dict()
  storage_config[storage_type] = shelvefile
  #Prep work - construct hash and features
  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config=storage_config,
   matrices_filename=matrix_filename, overwrite=True, private_config=privacy_config)
  feature_list = read_features(remainders[0])[:num_runs]
  feature_file.close()
  num_runs = min(num_runs, len(feature_list))
  #Start hashing
  
  for pair in feature_list:
    hash_start = time.time()
    hashObj.hash_test(pair[1])
    hash_duration = (time.time()-hash_start)*1000
    print(str(hash_duration))
     
  

if __name__ == "__main__":
  main()  
