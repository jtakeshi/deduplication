from __future__ import print_function
#from scipy.sparse import csr_matrix
import getopt
import sys
import time
from LLSH import LLSH
import pickle
import os

from list_features import get_model
from list_features import image_features


#Returns an array of 2-tuples - first entry is filename, second is feature vector
def read_features(input_filename, useCSR=False):
  print("Reading features from " + input_filename)
  f = open(input_filename,'r')
  lines = f.readlines()
  tuples = []
  for line in lines:
    #vals = line.split(' ')
    if line[0] == '#':
      continue
    split_line = line.split()
    fname = split_line[0]
    numbers = [float(i) for i in split_line[1:]]
    """
    try:
      numbers = [float(i) for i in split_line[1:]]
    except:
      print("Error at line: " + line)
      sys.exit(1)  
    """  
    """
    if useCSR:
      numbers = csr_matrix(numbers)
    """  
    values_tuple = (fname, numbers)
    tuples.append(values_tuple)
  return tuples  

  #Assumes hashObj is already initialized  
  #features is an iterable object, not a filename
def build_index(features, hashObj, debug=False):
  num_hashed = 0
  for vec in features:
    #Debugging inputs to hashobj index
    #print(str(vec[1].size), vec[0])
    #pdb.set_trace()

    try:
      hashObj.index(vec[1], vec[0])
    except IndexError:
      print("IndexError: vector is: " + str(vec)) 
 
    #hashObj.index(vec[1], vec[0])
    if debug:
      num_hashed = num_hashed+1
      print(str(num_hashed) + " vectors hashed")
  return hashObj
  
'''
def construct_index(feature_filename, hsize, n_features, n_tables,
  debug=False, persistence=None, planes_filename=None, private_config=None):
  hashObj = LLSH(hsize, n_features, n_tables, storage_config=persistence,
   matrices_filename=planes_filename, overwrite=True, private_config=private_config)
  features = read_features(feature_filename, False) #Read into a csr_matrix if needed
  hashObj = build_index(features, hashObj, debug)
  #print("Hash table construction time: " + str((time.time() - hashtime)*1000))
  return hashObj
'''

#Returns a list of up to n filenames for similar candidates  
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  candidates = hashObj.query(featureVec, n)
  ret = [x[0] for x in candidates]
  #if isSparse:
    #print("Shape: " + ret.shape())
    #ret = ret[0][0]
  #numsimilars = len(ret)
  #print("#Time to calculate similarity for " + str(numsimilars) + " files: " + str(1000*(time.time() - sim_time)))
  return ret

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3  
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  #Use this to switch between dbm and shelve
  storage_type = 'dbm'

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  
  num_to_index_from_subdir = 0

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:n:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True    
    elif opt in ('-n'):
      num_to_index_from_subdir = int(arg)


  #Check inputs
  bad_input = False
  if matrix_filename == '':
    print("No matrices file given!")
    bad_input = True
  if shelvefile == '':  
    print("No database given!")
    bad_input = True    

  if bad_input:
    return    

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes

  base_dir = remainders[0]
  if base_dir[-1] != '/':
    base_dir += '/'
  first_level_subdirs = [base_dir + x + '/' for x in os.listdir(base_dir) if os.path.isdir(base_dir + x + '/')]
  #DEBUG
  #print(first_level_subdirs)
  images = list()
  for subdir in first_level_subdirs:
    second_level_files = [x for x in os.listdir(subdir) if x[-4:] == '.jpg']
    if num_to_index_from_subdir > 0:
      second_level_files = second_level_files[:num_to_index_from_subdir]
    #DEBUG
    #print(second_level_files)
    for s in second_level_files:
      images.append(subdir + s)
      #DEBUG
      #print(images[-1])
      
      
  storage_config = dict()
  storage_config[storage_type] = shelvefile    
  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=True, private_config=privacy_config)
  model = get_model()    
      
  for filename in images:
    features = image_features(filename, model)
    hashObj.index(features, filename)
    print(filename)
    

  hashObj.close()
 
  

if __name__ == "__main__":
  main()  
