from LLSH import LLSH
import time
import sys
import getopt



#Returns a list of pairs of (up to n) filenames and collisions 
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  #candidates = hashObj.query(featureVec, n)
  #ret = [x[0] for x in candidates]
  #if isSparse:
    #print("Shape: " + ret.shape())
    #ret = ret[0][0]
  #numsimilars = len(ret)
  #print("#Time to calculate similarity for " + str(numsimilars) + " files: " + str(1000*(time.time() - sim_time)))
  #return ret
  return hashObj.query(featureVec, n)

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3 
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  persistence_type = 'dbm'
  #num_errors = 1
  query_filename = ''
  num_queries=0
  remove_substrings = True

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  ratio = 0

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:f:n:y')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True       
    elif opt in ('-f'):
      query_filename = arg
    elif opt in ('-n'):
      num_queries = int(arg)  
    elif opt in ('-y'):
      remove_substrings = True if arg not in ('false', 'False', 'FALSE', '0', 'no-privacy') else False       

  #Check inputs
  bad_input = False
  if matrix_filename == '':
  	print("No matrices file given!")
  	bad_input = True
  if shelvefile == '':	
  	print("No database given!")
  	bad_input = True    

  if bad_input:
  	return
  	
  #Use only an input file
  infile = open(query_filename, "r") 
  input_list = [x for x in infile.read().splitlines() if x[0] != '#']
  filenames = [line.rstrip().partition(' ')[0] for line in input_list]
  input_list = [[float(fval) for fval in line.rstrip().partition(' ')[2].split(' ')] for line in input_list]
  infile.close()
  #Limit the number of queries
  if num_queries > 0:
    input_list = input_list[:num_queries]
    filenames = filenames[:num_queries]
  num_queries = len(input_list)  

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes	

  storage_config = dict()
  storage_config[persistence_type] = shelvefile    

  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=False, private_config=privacy_config)
  
  
  false_positives = [0]*(num_tables-1)
  correct_positives = [0]*(num_tables-1)
  false_negatives = [0]*(num_tables-1)
  correct_negatives = [0]*(num_tables-1)
  empty_results = 0
  for idx, features in enumerate(input_list):
    query_result = hashObj.query(features)
    if not query_result:
      empty_results += 1
      continue
    for result in query_result:
      for k in range(1, num_tables+1):
        pos_arr_idx = k-1
        if result[0] == filenames[idx]:
          if result[1] < k:
            false_negatives[pos_arr_idx] += 1
          else:
            correct_positives[pos_arr_idx] += 1  
        else:
          if result[1] >= k:
            false_positives[pos_arr_idx] += 1
          else:
            correct_negatives[pos_arr_idx] += 1  
              
      
  #Print report
  print("Queried " + str(len(input_list)) + " images")
  if empty_results > 0:
    print(str(empty_results) + " empty returns from list")
  print("k,false_negatives,false_negatives_percent,false_positives,false_positives_percent,correct_pos,correct_neg")
  for i in range(len(false_positives)):
    print(str(i+1) + ',' + str(false_negatives[i]) + ',' + str(float(false_negatives[i])/float(num_queries)) + ',' + str(false_positives[i]) + ',' + str(float(false_positives[i])/float(num_queries)) + ',' + str(correct_positives[i]) + ',' + str(correct_negatives[i]))

if __name__ == "__main__":
  main()  
