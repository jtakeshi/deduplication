#from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import axes3d
from matplotlib import style
import matplotlib.pyplot as plt
import numpy as np

import sys

style.use('ggplot')

#DISTORTION = 'blur'
NUM_TABLES = 6
TABLE_INDICES = [i for i in range(NUM_TABLES+1)]
BLUR_PARMS = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
blur_data = ([1,3,1,33,158,500,1285]*5) + [0,3,4,34,164,559,1216] + [3,7,32,141,433,774,590] + [22,105,310,506,581,332,124] + [227,513,565,407,201,59,8] + [1116,523,223,84,23,10,1]
#Do percentage
TOTAL_QUERIES = 50
blur_data = [float(x)/float(TOTAL_QUERIES) for x in blur_data]

def get_parms(distortion, sort=True):
  ret = None
  if distortion == 'blur':
    ret = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
  elif distortion == 'brighten':
    ret = [0.1*x for x in range(1, 11)]
  elif distortion == 'gauss':
    ret = [0.1*x for x in range(1, 10)]
  elif distortion == 'resize':
    ret = [0.995, 0.99125, 1.0075, 0.9975, 0.99375, 1.00857, 1.00125, 0.99625, 0.99, 0.9925, 1.00625, 1.0025, 1.005, 1.00375, 1.01, 0.99857]
  elif distortion == 'saturate':
    ret = [0.5*x for x in range(1, 11)]
  elif distortion == 'sharpen':
    ret = [0.1*x for x in range(1, 6)]
  elif distortion == 'solarize':
    ret = [0.001*x for x in range(1, 11)]
  elif distortion == 'sp':
    ret = [0.001*x for x in range(1, 10)]
  if sort:
    ret.sort()
  return ret

#Order these in the same order as the parms
def get_data(distortion):
  need_to_sort = False
  if distortion in ['resize']:
    need_to_sort = True
  if distortion == 'blur':
    return ([1,3,1,33,158,500,1285]*5) + [0,3,4,34,164,559,1216] + [3,7,32,141,433,774,590] + [22,105,310,506,581,332,124] + [227,513,565,407,201,59,8] + [1116,523,223,84,23,10,1]
  elif distortion == 'brighten':
    return [1,3,0,25,154,509,1288] + [0,1,8,49,206,587,1129] + [0,3,22,81,302,701,871] + [0,4,27,144,361,684,760] + [0,8,29,132,396,747,668] + [0,4,32,150,401,741,652] + [0,9,29,173,426,742,601] + [1,11,35,177,422,715,619] + [0,8,47,169,436,754,566] + [0,7,37,193,440,782,521]
  elif distortion == 'gauss':
    return [338,577,502,334,155,54,20] + [331,570,541,331,137,61,9] + [358,544,559,310,147,53,9] + [344,598,531,323,123,56,5] + [341,601,499,347,142,42,8] + [334,619,534,295,139,51,8] + [337,630,520,316,129,38,10] + [358,580,541,314,140,43,4] + [371,588,547,301,110,58,5]
  elif distortion == 'resize':
    data = [[682, 718, 367, 160, 46, 7, 0], [682, 720, 370, 159, 42, 7, 0], [4, 15, 11, 37, 160, 492, 1261], [682, 719, 366, 161, 45, 7, 0], [681, 718, 369, 159, 46, 7, 0], [20, 38, 22, 40,159,474,1227], [1,3,1,33,158,500,1284], [682,718,367,161,45,7,0], [669,715,385,160,42,9,0], [678,721,372,156,46,7,0], [2,6,6,35,160,496,1275], [1,3,1,33,158,500,1284], [1,4,4,33,158,499,1281], [1,4,2,33,158,500,1282], [315,258,141,66,98,288,814], [682,719,366,161,45,7,0]]
  elif distortion == 'saturate':
    return [1,3,1,34,161,494,1286] + [0,2,8,54,187,574,1155] + [0,3,12,75,256,649,985] + [0,3,20,119,320,675,843] + [0,4,23,133,385,718,717] + [0,7,41,153,390,737,652] + [1,11,44,180,448,692,604] + [0,14,43,223,479,712,509] + [1,13,56,224,481,723,482] + [1,14,71,255,513,703,423]
  elif distortion == 'sharpen':
    return [1,3,1,33,158,500,1284]*2 + [0,4,1,37,146,517,1275] + [1,12,51,176,519,744,477] + [68,278,486,564,386,157,41]
  elif distortion == 'solarize':
    return [916,254,149,122,81,135,323]*3 + [968,253,134,99,84,131,311]*4 + [1009,239,144,96,79,118,295]*3
  elif distortion =='sp':
    return [861, 670,313,100,31,5,0] + [1255,510,157,48,10,0,0] + [1422,405,122,29,2,0,0] + [1551,326,87,15,1,0,0] + [1617, 302,45,13,3,0,0] + [1683,244,46,6,1,0,0] + [1744,172,25,3,0,0,0] + [1780,172,25,3,0,0,0] + [1795,153,27,4,1,0,0]
  if need_to_sort:
    dist_parms = get_parms(distortion, sort=False)
    zipped = zip(dist_parms, data)
    zipped.sort(key=lambda x: x[0])
    ret = list()
    for x in zipped:
      ret += x[1]
    return ret
  return data

def get_data_percentage(distortion):
  raw_data = get_data(distortion)
  total = float(np.sum(raw_data))/float(len(get_parms(distortion)))
  return [float(x)/float(total) for x in raw_data]

#Return (dx, dy) for a distortion - based on the data
def get_widths(distortion):
  X_DEFAULT = 0.5
  if distortion == 'blur':
    return (X_DEFAULT, 0.025)
  elif distortion == 'brighten' or distortion == 'gauss' or distortion == 'sharpen':
    return (X_DEFAULT, 0.05)
  elif distortion == 'resize':
    return (X_DEFAULT, 0.00125)
  elif distortion == 'saturate':
    return (X_DEFAULT, 0.25)
  elif distortion == 'solarize' or distortion == 'sp':
    return (X_DEFAULT, 0.0005)
  else:
    return (X_DEFAULT, 0.05) #Default

'''
parms_list = list()
for i in BLUR_PARMS:
  parms_list += [i]*len(TABLE_INDICES)
'''  
def main():  
  if len(sys.argv) < 2:
    print("Argument required")
    return
  distortion = sys.argv[1]
  if distortion not in ['blur', 'brighten', 'gauss', 'resize', 'saturate', 'sharpen', 'solarize', 'sp']:
    print("Invalid argument: " + distortion)
    return

  fig = plt.figure()
  ax1 = fig.add_subplot(111, projection='3d')

  pct_data = get_data_percentage(distortion)
  parms = get_parms(distortion)

  num_points = len(pct_data)
  if num_points != len(TABLE_INDICES) * len(parms):
    print("Points: " + str(num_points))
    print("Parameters: " + str(len(parms)))
    assert(num_points == len(TABLE_INDICES) * len(parms))

  x3 = TABLE_INDICES*len(parms)
  y3 = list()
  for i in parms:
    y3 += [i]*len(TABLE_INDICES)
  z3 = np.zeros(num_points)

  dx, dy = get_widths(distortion)
  dz = pct_data


  '''
  graph_colors = list()
  for c in ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w', '0.5', 'C0']:
    graph_colors += [c]*len(TABLE_INDICES)
  '''  
  #['b', 'g', 'r', 'w', 'c', 'm', 'y', '0.5', 'C0', 'k']
  color_inv = 1.0/len(parms)
  BASIC_COLORS = [(color_inv*x, color_inv*x, 1.0-(color_inv*x)) for x in range(len(parms))]
  BASIC_COLORS = list(reversed(BASIC_COLORS))
  #DEBUG
  #print(BASIC_COLORS)
  #1 color for each parameter
  assert(len(BASIC_COLORS) == len(parms))
  graph_colors = list()
  for c in BASIC_COLORS:
    graph_colors += [c]*len(TABLE_INDICES)

  assert(len(graph_colors) == num_points)
    
  ax1.bar3d(x3, y3, z3, dx, dy, dz, color=graph_colors)
  ax1.set_xlabel('Hits')
  ax1.set_ylabel('Parameter')
  ax1.set_zlabel('Proportion of Images')
  plt.title('Distortion effects for ' + distortion)
  ax1.view_init(azim=ax1.azim-60)
  #plt.gca().invert_yaxis()
  plt.show()
  #plt.savefig('blur', bbox_inches='tight')

if __name__ == "__main__":
  main()
