from __future__ import print_function
import getopt
import sys
import time
from LLSH import LLSH
import pickle
from multiprocessing.pool import ThreadPool as Pool
import numpy as np
from scipy import stats


#Returns an array of 2-tuples - first entry is filename, second is feature vector
def read_features(input_filename, useCSR=False):
  print("Reading features from " + input_filename)
  f = open(input_filename,'r')
  lines = f.readlines()
  tuples = []
  for line in lines:
    #vals = line.split(' ')
    if line[0] == '#':
      continue
    split_line = line.split()
    fname = split_line[0]
    numbers = [float(i) for i in split_line[1:]]

    values_tuple = (fname, numbers)
    tuples.append(values_tuple)
  return tuples  

  #Assumes hashObj is already initialized  
  #features is an iterable object, not a filename
def build_index(features, hashObj, debug=False):
  num_hashed = 0
  for vec in features:
    try:
      hashObj.index(vec[1], vec[0])
    except IndexError:
      print("IndexError: vector is: " + str(vec)) 
 
    #hashObj.index(vec[1], vec[0])
    if debug:
      num_hashed = num_hashed+1
      print(str(num_hashed) + " vectors hashed")
  return hashObj
  
def time_index(args):
  hashes = args[0]
  hashObj = args[1]
  threading = True if (len(hashes) > 1) else False
  extra_data = 0
  start_time = time.time()  
  hashObj.index_nohash(hashes, extra_data, threading)
  end_time = time.time()
  return start_time - end_time
  
def get_hash(feature_filename, hsize, n_features, n_tables,
  debug=False, persistence=None, planes_filename=None, private_config=None):
  hashObj = LLSH(hsize, n_features, n_tables, storage_config=persistence,
   matrices_filename=planes_filename, overwrite=True, private_config=private_config)
  features = read_features(feature_filename, False) #Read into a csr_matrix if needed
  hashObj = build_index(features, hashObj, debug)
  #print("Hash table construction time: " + str((time.time() - hashtime)*1000))
  return hashObj

#Returns a list of up to n filenames for similar candidates  
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  candidates = hashObj.query(featureVec, n)
  ret = [x[0] for x in candidates]
  return ret

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3  
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  #Use this to switch between dbm and shelve
  storage_type = 'dbm'
  MAX_THREADS = 16384

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  
  num_queries_list = list()

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:n:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True   
    elif opt in ('-n'):
      num_queries_list = [int(x) for x in arg.strip().split(':')] #Argue queries as a quote-contained whitespace-delimited list of integers
      #Split num. queries by colon for easy arg. parsing
      num_queries_list.sort()


  #Check inputs
  bad_input = False
  if matrix_filename == '':
    print("No matrices file given!")
    bad_input = True
  if shelvefile == '':  
    print("No database given!")
    bad_input = True    

  if bad_input:
    return    

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes

  feature_file = remainders[0]
  #print("#Reading features from " + feature_file)    
  
  infile = open(feature_file, "r") 
  #input_list = infile.read().splitlines()
  input_list = []
  read_in = 0
  while read_in < max(num_queries_list):
    instr = next(infile)
    if instr is None:
      break
    if instr[0] != '#':
      input_list.append([float(fval) for fval in instr.rstrip().partition(' ')[2].split(' ')]) #Read and process input one line at a time
      #input_list.append(instr)
      read_in += 1
  #input_list = [next(infile) for x in range(num_queries)]
  infile.close()
  #Skip filename as first entry
  #input_list = [[float(fval) for fval in fn.rstrip().partition(' ')[2].split(' ')] for fn in input_list if fn[0] != '#']
 
  #DEBUG
  print("Read input file")
 
  storage_config = dict()
  storage_config[storage_type] = shelvefile
  
  #Create one LLSH obj. to compute hashes
  #Do this with a pool for speed
  preprocess_pool = False
  
  tmp_hash = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config,
     matrix_filename, overwrite=True, private_config=privacy_config)
  input_hashes = list()  
  if preprocess_pool:   
    tmp_pool = Pool(processes=min(len(input_list), MAX_THREADS))
    input_hashes = [x for x in tmp_pool.imap_unordered(tmp_hash.query_hashes, (f for f in input_list))]
    tmp_pool.close()
    tmp_pool.join()
  else:
    input_hashes = [tmp_hash.query_hashes(f) for f in input_list]
  
  #input_hashes = [tmp_hash.query_hashes(f) for f in input_list]
  tmp_hash.close()
  
  #DEBUG  
  print("Computed Hashes")  


  for num_queries in num_queries_list:
    hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config,
     matrix_filename, overwrite=True, private_config=privacy_config, multithreaded=(True if (num_queries > 1) else False))
    #query_hashes = input_hashes[:num_queries]
    #Throw off a pool to extract features
    pool = Pool(processes=min(num_queries, MAX_THREADS, len(input_hashes)))
    total_start = time.time()
    #DEBUG
    #timing_results = [hashObj.index_nohash_time(h) for h in input_hashes[:num_queries]]
    timing_results = [x for x in pool.imap_unordered(hashObj.index_nohash_time, iterable=(h for h in input_hashes[:num_queries]))]
    total_end = time.time()
    total_duration = total_end - total_start
    pool.close()
    pool.join()
    #Flush hash data to a shelve file
    hashObj.close()
    #Print results in .csv format
    thread_avg = np.average(timing_results)
    thread_std = np.std(timing_results)
    thread_mode = stats.mode(timing_results)[0][0]
    thread_median = np.median(timing_results)
    print(str(num_queries) + ',' + str(thread_avg) + ',' + str(thread_std) + ',' + str(thread_mode) + ',' + str(thread_median) + ',' + str(total_duration))
    
  return


if __name__ == "__main__":
  main()  
