from LLSH import LLSH
import time
import sys
import getopt
#from multiprocessing import Pool
#from pathos.multiprocessing import ProcessingPool as Pool
from multiprocessing.pool import ThreadPool as Pool
import numpy as np
from scipy import stats



#Returns a list of pairs of (up to n) filenames and collisions 
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  #candidates = hashObj.query(featureVec, n)
  #ret = [x[0] for x in candidates]
  #if isSparse:
    #print("Shape: " + ret.shape())
    #ret = ret[0][0]
  #numsimilars = len(ret)
  #print("#Time to calculate similarity for " + str(numsimilars) + " files: " + str(1000*(time.time() - sim_time)))
  #return ret
  return hashObj.query(featureVec, n)
  
def time_query(args, n=None, isSparse=False):
  #return hashObj.query(featureVec, n)
  featureVec = args[0]
  hashObj = args[1]
  start = time.time()
  result = hashObj.query_nohash(featureVec, n)
  end = time.time()
  return end-start

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3 
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  persistence_type = 'dbm'
  num_errors = 1
  query_filename = ''
  num_queries_list = list()
  num_threads = 4
  MAX_THREADS = 16384

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  ratio = 0

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:e:f:n:z:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True       
    elif opt in ('-e'):
      num_errors = int(arg)  
    elif opt in ('-f'):
      query_filename = arg
    elif opt in ('-n'):
      num_queries_list = [int(x) for x in arg.strip().split(':')] #Argue queries as a quote-contained whitespace-delimited list of integers
      #Split num. queries by colon for easy arg. parsing
      num_queries_list.sort() 
       
    
    
   
      

  #Check inputs
  bad_input = False
  if matrix_filename == '':
  	print("No matrices file given!")
  	bad_input = True
  if shelvefile == '':	
  	print("No database given!")
  	bad_input = True    

  if bad_input:
  	return
  	
  #Decide whether to use CLI filenames or file-based filenames
  #One or the other for now - could allow both
  input_list = None
  if query_filename == '':
    input_list = remainders
  else:
    infile = open(query_filename, "r") 
    #input_list = infile.read().splitlines()
    input_list = []
    read_in = 0
    while read_in < max(num_queries_list):
      instr = next(infile)
      if instr is None:
        break
      if instr[0] != '#':
        input_list.append(instr)
        read_in += 1
    #input_list = [next(infile) for x in range(num_queries)]
    infile.close()
  input_list = [[float(fval) for fval in fn.rstrip().partition(' ')[2].split(' ')] for fn in input_list if fn[0] != '#']
  #Should be arguing features (with filename, so strip first thing)
  
  #DEBUG
  #print(input_list[0])

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes	


  storage_config = dict()
  #TODO update naming
  storage_config[persistence_type] = shelvefile    

  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=False, private_config=privacy_config)
  input_hashes = [hashObj.query_hashes(f) for f in input_list]
 
  
  for num_queries in num_queries_list:
    query_hashes = input_hashes[:num_queries]
    #Throw off a pool to extract features
    pool = Pool(processes=min(len(query_hashes), MAX_THREADS))
    total_start = time.time()
    timing_results = [x for x in pool.imap_unordered(time_query, iterable=[(h, hashObj) for h in query_hashes])]
    total_end = time.time()
    total_duration = total_end - total_start
    pool.close()
    pool.join()
    #Print results in .csv format
    thread_avg = np.average(timing_results)
    thread_std = np.std(timing_results)
    thread_mode = stats.mode(timing_results)[0][0]
    thread_median = np.median(timing_results)
    print(str(num_queries) + ',' + str(thread_avg) + ',' + str(thread_std) + ',' + str(thread_mode) + ',' + str(thread_median) + ',' + str(total_duration))


  hashObj.close()
  return




if __name__ == "__main__":
  main()  
