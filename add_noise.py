#!/bin/python

#https://stackoverflow.com/questions/22937589/how-to-add-noise-gaussian-salt-and-pepper-etc-to-image-in-python-with-opencv


#$python add_noise.py -n gauss -v 300.0 -m 0 images/ggb_original.jpg

import sys
import getopt

"""
    The Function adds gaussian , salt-pepper , poisson and speckle noise in an image

Parameters
----------
image : ndarray
    Input image data. Will be converted to float.
mode : str
    One of the following strings, selecting the type of noise to add:

    'gauss'     Gaussian-distributed additive noise.
    'poisson'   Poisson-distributed noise generated from the data.
    's&p'       Replaces random pixels with 0 or 1.
    'speckle'   Multiplicative noise using out = image + n*image,where
                n is uniform noise with specified mean & variance.
"""

import numpy as np
import os
import cv2
def noisy(noise_typ,image,params):
   if noise_typ == "gauss":
      row,col,ch= image.shape
      mean = params["mean"]
      var = params["variance"]
      sigma = var**0.5
      gauss = np.random.normal(mean,sigma,(row,col,ch))
      gauss = gauss.reshape(row,col,ch)
      noisy = image + gauss
      return noisy
   elif noise_typ == "sp":
      row,col,ch = image.shape
      s_vs_p = params["sp"]
      amount = params["amount"]
      out = np.copy(image)
      # Salt mode
      num_salt = np.ceil(amount * image.size * s_vs_p)
      coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
      out[coords] = 1

      # Pepper mode
      num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
      coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
      out[coords] = 0
      return out
   elif noise_typ == "poisson":
      vals = len(np.unique(image))
      vals = 2 ** np.ceil(np.log2(vals))
      noisy = np.random.poisson(image * vals) / float(vals)
      return noisy
   elif noise_typ == "speckle":
      row,col,ch = image.shape
      gauss = np.random.randn(row,col,ch)
      gauss = gauss.reshape(row,col,ch)        
      noisy = image + image * gauss
      return noisy


def main():
  noise_list = ["gauss", "sp", "poisson", "speckle"]
  noise_params = dict()
  noise_typ = ''
  grayscale = False
  output_filename = ''

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'n:v:s:m:b:a:o:g')

  for opt, arg in options:
    if opt in ('-n'):
      if arg in noise_list:
        noise_typ = arg
      else:  
        print("Invalid noise type!")
        sys.exit(0)
    elif opt in ('-v'):
      noise_params["variance"] = float(arg) 
    elif opt in ('-m'):
      noise_params["mean"] = float(arg)
    elif opt in ('-s'):
      noise_params["sp"] = float(arg)
    elif opt in ('-a'):
      noise_params["amount"] = float(arg)  
    elif opt in ('-g'):
      grayscale = True
    elif opt in ('-o'):
      output_filename = arg  

  #Error check!
  if noise_typ == '':
  	print("Must specify a noise type!")
  	sys.exit(0)    


  for filename in remainders:
    split_filename = filename.split(".")
    #Add .noisy to the middle of the filename - have to do this because opencv requires a meaningful extension
    if output_filename == '':
      output_filename = split_filename[0] + "_noisy." + split_filename[1]
    original_img = cv2.imread(filename)    
    noisy_img = noisy(noise_typ, original_img, noise_params)
    #Convert to gray - should be only used for putting things in paper
    if grayscale:
      #DEBUG:
      print(noisy_img.shape)
      noisy_img = cv2.cvtColor(noisy_img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(output_filename, noisy_img)

  print("#Added noise to " + str(len(remainders)) + " images")







if __name__ == "__main__":
  main()


