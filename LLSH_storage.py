# lshash/storage.py
# Copyright 2012 Kay Zhu (a.k.a He Zhu) and contributors (see CONTRIBUTORS.txt)
#
# This module is part of lshash and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php

import json
import shelve
import dbm

try:
    import redis
except ImportError:
    redis = None

__all__ = ['storage']


#Debug var here - TODO remove
def storage(storage_config, index, debug=False):
    """ Given the configuration for storage and the index, return the
    configured storage instance.
    """
    if 'dict' in storage_config:
        return InMemoryStorage(storage_config['dict'])
    elif 'redis' in storage_config:
        storage_config['redis']['db'] = index
        return RedisStorage(storage_config['redis'])
    elif 'shelve' in storage_config:
        return ShelveStorage(storage_config['shelve']+'.'+str(index), debug)   
    elif 'dbm' in storage_config:
        return DBMStorage(storage_config['dbm'] + '.' + str(index))
    else:
        raise ValueError("Unsupported method!")


class BaseStorage(object):
    def __init__(self, config):
        """ An abstract class used as an adapter for storages. """
        raise NotImplementedError

    def keys(self):
        """ Returns a list of binary hashes that are used as dict keys. """
        raise NotImplementedError

    def set_val(self, key, val):
        """ Set `val` at `key`, note that the `val` must be a string. """
        raise NotImplementedError

    def get_val(self, key):
        """ Return `val` at `key`, note that the `val` must be a string. """
        raise NotImplementedError

    def append_val(self, key, val):
        """ Append `val` to the list stored at `key`.

        If the key is not yet present in storage, create a list with `val` at
        `key`.
        """
        raise NotImplementedError

    def get_list(self, key):
        """ Returns a list stored in storage at `key`.

        This method should return a list of values stored at `key`. `[]` should
        be returned if the list is empty or if `key` is not present in storage.
        """
        raise NotImplementedError


class InMemoryStorage(BaseStorage):
    def __init__(self, config):
        self.name = 'dict'
        self.storage = dict()

    def keys(self):
        return self.storage.keys()

    def set_val(self, key, val):
        self.storage[key] = val

    def get_val(self, key):
        return self.storage[key]

    def append_val(self, key, val):
        self.storage.setdefault(key, []).append(val)

    def get_list(self, key):
        return self.storage.get(key, [])


class ShelveStorage(BaseStorage):
    def __init__(self, config, overwrite=False, debug=False):
        self.name = 'shelve'
        if debug:
            print("Opened shelve file " + config)
        self.storage = shelve.open(config)
        self.filename = config

    def keys(self):
        return self.storage.keys()

    def set_val(self, key, val):
        self.storage[key] = val

    def get_val(self, key):
        return self.storage[key]

    def append_val(self, key, val, debug=False):
        if debug:
            print("Appending value " + str(val))
        #Very inefficient...    
        #We need this because shelve can't detect when mutable objects (e.g. lists) nested under a key are changed.

        if key in self.storage.keys():
            storage_list = self.storage[key]
            if not val in storage_list:
                storage_list.append(val)
                self.storage[key] = storage_list
        else:
            storage_list = []
            storage_list.append(val)
            self.storage[key] = storage_list   
        #self.storage.setdefault(key, []).append(val)

    def get_list(self, key):
        return self.storage.get(key, [])

    def open(self, filename=None):
        if filename is not None:
            self.storage = shelve.open(filename)
            self.filename = filename
        else:
            self.storage = shelve.open(self.filename)    

    def close(self):
        self.storage.close()

def string_to_list(s, delim=','):
    return s.split(delim)
def list_to_string(l, delim=','):
    return delim.join(l)      

class DBMStorage(BaseStorage):
    def __init__(self, config, overwrite=False, delim=','):
        self.name = 'DBM'
        #self.storage = dbm.open(config, filename)
        self.filename = config
        self.delim = delim
        if overwrite:
            self.storage = dbm.open(config, 'n')
        else:
            self.storage = dbm.open(config, 'c')    
    def keys(self):
        return self.storage.keys()
    def set_val(self, key, val):
        self.storage[key] = val
    def get_val(self, key):
        if key not in self.keys():
          return None
        sl = string_to_list(self.storage[key], self.delim)
        sl_stripped = [item for item in sl if item != '']
        return sl_stripped
    def get_list(self, key):
        #Might have to revise this - does DBM have .get()?
        return self.get_val(key)
    def append_val(self, key, val):
        if key in self.storage.keys():
            storage_list = self.get_list(key)
            #Left off here
            if not val in storage_list:
                storage_list.append(val + self.delim)
                self.storage[key] = list_to_string(storage_list, self.delim)
        else:
            self.storage[key] = val + self.delim 
    def close(self):
        self.storage.close()
    def open(config='c'):
        self.storage = dbm.open(self.filename, config)            

class RedisStorage(BaseStorage):
    def __init__(self, config):
        if not redis:
            raise ImportError("redis-py is required to use Redis as storage.")
        self.name = 'redis'
        self.storage = redis.StrictRedis(**config)

    def keys(self, pattern="*"):
        return self.storage.keys(pattern)

    def set_val(self, key, val):
        self.storage.set(key, val)

    def get_val(self, key):
        return self.storage.get(key)

    def append_val(self, key, val):
        self.storage.rpush(key, json.dumps(val))

    def get_list(self, key):
        return self.storage.lrange(key, 0, -1)
