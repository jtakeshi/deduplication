from __future__ import print_function
#from scipy.sparse import csr_matrix
import getopt
import sys
import time
from LLSH import LLSH
import pickle


#Returns an array of 2-tuples - first entry is filename, second is feature vector
def read_features(input_filename, useCSR=False):
  print("Reading features from " + input_filename)
  f = open(input_filename,'r')
  lines = f.readlines()
  tuples = []
  for line in lines:
    #vals = line.split(' ')
    if line[0] == '#':
      continue
    split_line = line.split()
    fname = split_line[0]
    numbers = [float(i) for i in split_line[1:]]
    """
    try:
      numbers = [float(i) for i in split_line[1:]]
    except:
      print("Error at line: " + line)
      sys.exit(1)  
    """  
    """
    if useCSR:
      numbers = csr_matrix(numbers)
    """  
    values_tuple = (fname, numbers)
    tuples.append(values_tuple)
  return tuples  

  #Assumes hashObj is already initialized  
  #features is an iterable object, not a filename
def build_index(features, hashObj, debug=False):
  num_hashed = 0
  for vec in features:
    #Debugging inputs to hashobj index
    #print(str(vec[1].size), vec[0])
    #pdb.set_trace()

    try:
      hashObj.index(vec[1], vec[0])
    except IndexError:
      print("IndexError: vector is: " + str(vec)) 
 
    #hashObj.index(vec[1], vec[0])
    if debug:
      num_hashed = num_hashed+1
      print(str(num_hashed) + " vectors hashed")
  return hashObj
  
def get_hash(feature_filename, hsize, n_features, n_tables,
  debug=False, persistence=None, planes_filename=None, private_config=None):
  hashObj = LLSH(hsize, n_features, n_tables, storage_config=persistence,
   matrices_filename=planes_filename, overwrite=True, private_config=private_config)
  features = read_features(feature_filename, False) #Read into a csr_matrix if needed
  hashObj = build_index(features, hashObj, debug)
  #print("Hash table construction time: " + str((time.time() - hashtime)*1000))
  return hashObj

#Returns a list of up to n filenames for similar candidates  
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  candidates = hashObj.query(featureVec, n)
  ret = [x[0] for x in candidates]
  #if isSparse:
    #print("Shape: " + ret.shape())
    #ret = ret[0][0]
  #numsimilars = len(ret)
  #print("#Time to calculate similarity for " + str(numsimilars) + " files: " + str(1000*(time.time() - sim_time)))
  return ret

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3  
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  #Use this to switch between dbm and shelve
  storage_type = 'dbm'

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True      


  #Check inputs
  bad_input = False
  if matrix_filename == '':
    print("No matrices file given!")
    bad_input = True
  if shelvefile == '':  
    print("No database given!")
    bad_input = True    

  if bad_input:
    return    

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes

  feature_file = remainders[0]
  #print("#Reading features from " + feature_file)    
  storage_config = dict()
  storage_config[storage_type] = shelvefile
  hash_start = time.time()
  hashObj = get_hash(feature_file, hash_size, NUM_FEATURES, 
    num_tables, debug=False, persistence=storage_config, planes_filename=matrix_filename, private_config=privacy_config)	
  #Done with hashing, save time now
  hash_duration = (time.time()-hash_start)*1000
  #Flush hash data to a shelve file
  hashObj.close()
  print("#Data flushed to "+shelvefile+'.*')
  print("#Indexed " + str(hashObj.num_indexed()) + " items from " + feature_file)
  print("#Time to build hash: " + str(hash_duration) + " ms")

  
  #Debugging - test image similarity
  #print("Hash table: ")
  #print(str(hashObj))

  """
  image_filenames = remainders[1:]
  feature_vec = dict()
  model = get_model()
  for filename in image_filenames:
    feature_vec[filename] = image_features(filename, model)
    similars = similar_files(feature_vec[filename], hashObj)
    if len(similars) > 0:
      print("Files similar to " + filename + ":")
      for s in similars:
        print("\t" + s)
    else:
      print("No similar files for " + filename) 
  """
  

if __name__ == "__main__":
  main()  
