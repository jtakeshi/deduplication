#Program to list features
#Input: file containing image filenames. List with $ ls -d $PWD/*.jpg or similar
#Output: each line in the output consists of a filename, followed by the feature vector (all space-delimited)
from __future__ import print_function
import os
#Disable logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.applications.resnet50 import preprocess_input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import Model
import numpy as np
import sys
import getopt

import time


IMAGESIZE = 224



def image_features(filename, model):
  feat_start = time.time()
  #DEBUG
  #print("Opening" + filename)
  img = image.load_img(filename, target_size=(IMAGESIZE, IMAGESIZE))
  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)
  x = preprocess_input(x)
  prediction = model.predict(x)
  #print(prediction.shape)
  p = prediction.flatten()
  #print("Feature calculation time: " + str((time.time()-feat_start)*1000) + " ms")
  return p
  '''
  arr = []
  for elt in prediction:
    arr.add(elt)
  return arr
  '''

def print_feature_array(arr):
  #first_elt = True
  for x in arr:
    '''
    if not first_elt:
      print(',',end=' ')
      first_elt = False
    '''
    print(x,end=' ')
    
def get_model(modelstr='ResNet50'):
  #init_time = time.time()
  if modelstr == 'VGG19':
    base_model = VGG19(weights='imagenet')
    model = Model(inputs=base_model.input, outputs=base_model.get_layer('fc2').output)
    #print("Time to init model: " + str((time.time()-init_time)*1000) + " ms")
    return model
  elif modelstr == 'ResNet50':
    base_model = ResNet50(weights='imagenet')
    #used model.summary() to guess at which layer is the 2048-dimensional feature vector
    model = Model(inputs=base_model.input, outputs=base_model.get_layer('avg_pool').output)
    return model
    



def main():
  #Model is too large without this
  os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

  
  model_start = time.time()
  model = get_model()
  print("#Time to init model: " + str((time.time()-model_start)*1000) + " ms")
 
  arguments = sys.argv[1:]
  if len(arguments) < 1:
    print("Provide a single filename argument, a file containing newline-delimited filenames")
    sys.exit(0)
  firstprint = True
  extraction_start = time.time()
  images_filename = arguments[0]
  num_processed = 0

  with open(images_filename) as infile:
    for line in infile:
      line = line.rstrip()
      if not firstprint:
        print('')
      else:
        firstprint = False
      print(line, end=' ')
      arr = image_features(line, model)
      print_feature_array(arr)    
      num_processed = num_processed + 1

  """
  for filename in arguments:
    arr = image_features(filename, model)
    #print("Array size: " + str(arr.shape))
    if not firstprint:
    #Need to print a newline as well, but not for the last one
      print('')
    else:
      firstprint = False
    print(filename, end=' ')
    print_feature_array(arr)
  """


  print("\n#Time to extract features for " + str(num_processed) + " images: " + str((time.time()-extraction_start)*1000) + " ms")
  
if __name__ == "__main__":
  main()

#JST
'''
print("Size")
print(block4_pool_features.size)
print("Shape:")
print(block4_pool_features.shape)
#print(block4_pool_features)

print("Flattened vector:")
flatvec = block4_pool_features.flatten()
for val in flatvec:
  print(val,end='')
'''
