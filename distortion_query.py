from LLSH import LLSH
from list_features import get_model
from list_features import image_features
import time
import sys
import getopt
import os
import numpy as np
 
#Original_file distortion_type param:filename ... 
def parse_line(line):
  lines = line.rstrip().split(' ')
  original_filename = lines[0]
  distortion_type = lines[1]
  distorted_images = [(x.split(':')[1], x.split(':')[0]) for x in lines[2:]]
  return (original_filename, distortion_type, distorted_images)

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3 
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  persistence_type = 'dbm'
  num_errors = 1
  query_filename = ''
  num_queries=0
  remove_substrings = True

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  ratio = 0
  
  distortion_type = ''
  #distortion_param = ''
  
  on_the_fly = False

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  #d is for distortion type, l is for level of distortion
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:f:n:y:d:o')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True       
    elif opt in ('-f'):
      query_filename = arg
    elif opt in ('-n'):
      num_queries = int(arg)  
    elif opt in ('-y'):
      remove_substrings = True if arg not in ('false', 'False', 'FALSE', '0', 'no-privacy') else False      
    elif opt in ('-d'):
      distortion_type = arg 
    elif opt in ('-o'):
      on_the_fly = True
      

  #Check inputs
  bad_input = False
  if matrix_filename == '':
  	print("No matrices file given!")
  	bad_input = True
  if shelvefile == '':	
  	print("No database given!")
  	bad_input = True    

  if bad_input:
  	return
  	
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes 	
    
  storage_config = dict()
  storage_config[persistence_type] = shelvefile   
  	
  f = open(query_filename, "r")
  input_lines = [x for x in f.read().splitlines() if x[0] != '#']
  f.close()
  
  query_lines = [parse_line(x) for x in input_lines]

  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=on_the_fly, private_config=privacy_config)
  model = get_model()
  
  '''  
  if on_the_fly:
    base_dir = remainders[0]
    if base_dir[-1] != '/':
      base_dir += '/'
    first_level_subdirs = [base_dir + x + '/' for x in os.listdir(base_dir) if os.path.isdir(base_dir + x + '/')]
    #DEBUG
    #print(first_level_subdirs)
    original_images = list()
    for subdir in first_level_subdirs:
      second_level_files = [x for x in os.listdir(subdir) if x[-4:] == '.jpg']
      #DEBUG
      #print(second_level_files)
      for s in second_level_files:
        original_images.append(subdir + s)
    for img in original_images:
      hashObj.index(image_features(img, model), img) 
  '''      
  if on_the_fly:
    for parsed in query_lines:
      original_img_name = parsed[0]
      hashObj.index(image_features(original_img_name, model), original_img_name)
  
  distortion_dict = dict()
  original_not_found = 0
  nonexistent_file = 0
  empty_query = 0
  total_distorted_queries = 0
  false_pos_dict = dict()
  #distortion_dict_arr = [dict()]*(num_tables+1)
  #For a parameter, how often will there be k \in [0, num_tables] table hits?
  if num_queries > 0:
    query_lines = query_lines[:num_queries]
  for linenum, parsed in enumerate(query_lines):
    if distortion_type != parsed[1]:
      print("ERROR: incorrect distortion type: " + distortion_type + " at line " + str(linenum))
    original_filename = parsed[0]
    distorted_files = parsed[2]
    #distorted_files.append((original_filename, 'NONE'))
    #Check for inclusion of the original filename
    original_query = hashObj.query(image_features(original_filename, model))
    if original_query:
      found = False
      for query_pair in original_query:
        if query_pair[0] == original_filename:
          found = True
          break
      if not found:    
        print("WARNING: original file not found in query: " + original_filename)
        original_not_found += 1
        continue
    else:
      print("WARNING: no query returned for original file: " + original_filename)    
      original_not_found += 1
      continue
    #Filename, parameter
    for distorted_pair in distorted_files:
      total_distorted_queries += 1
      distorted_fname = distorted_pair[0]
      if not os.path.isfile(distorted_fname):
        print("WARNING: file " + distorted_fname + " does not exist, continuing")
        nonexistent_file += 1
        continue
      if os.stat(distorted_fname).st_size == 0:
        print("WARNING: empty file: " +  distorted_fname + ", continuing")
        nonexistent_file += 1
        continue
      distortion_param = distorted_pair[1]
      query_result = hashObj.query(image_features(distorted_fname, model))
      if query_result is None:
        #print("WARNING: None returned for query: " + distorted_fname)
        empty_query += 1
        #continue
      if len(query_result) == 0:
        #print("WARNING: query returned empty: " + distorted_fname)
        empty_query += 1
        #continue
      #query_result = [x for x in query_result if x[1] > 0]
      #Force no false positives
      if query_result is None:
        query_result = list()
      match = [x for x in query_result if x[0] == original_filename]
      false_positives = [x for x in query_result if x[0] != original_filename]
      if false_positives:
        #print("WARNING: false positives: ")
        for fp in false_positives:
          #print("\t" + fp[0])
          if distortion_param not in false_pos_dict:
            false_pos_dict[distortion_param] = [0]*(num_tables+1)
            false_pos_dict[distortion_param][fp[1]] = 1
          else:
            false_pos_dict[distortion_param][fp[1]] += 1
      num_matches = match[0][1] if match else 0
      #if len(match) == 0:
        #print("WARNING: original not matched: " + distorted_fname)
      if len(match) > 1:
        print("WARNING: more than one identical match: " + distorted_fname)
      if distortion_param not in distortion_dict:
        distortion_dict[distortion_param] = [0]*(num_tables+1)
        distortion_dict[distortion_param][num_matches] = 1
      else:  
        distortion_dict[distortion_param][num_matches] += 1
      
  print("Distortion: " + distortion_type)
  for dist_type, dist_list in distortion_dict.items():
    print("Param: " + dist_type)
    list_tot = np.sum(dist_list)
    for idx, list_entry in enumerate(dist_list):
      print("\t" + str(idx) + " " + str(list_entry) + " " + str(float(list_entry)/float(list_tot)))

  print("\nFalse Positives: ")
  for dist_type, fp_list in false_pos_dict.items():
    print("Param: " + dist_type)
    for idx, list_entry in enumerate(fp_list):
      print('\t' + str(idx) + ' ' + str(list_entry))

  hashObj.close()
  
  print("Total dist. queries: " + str(total_distorted_queries))
  print("Original image not found: " + str(original_not_found))
  print("Nonexistent files: " + str(nonexistent_file))
  print("Empty query results: " + str(empty_query))

  	

if __name__ == "__main__":
  main()  	
