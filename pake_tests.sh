#!/bin/bash
OUTFILE=${1?Error: no output file given}
NUMREPS=${2?:Error: no number of repetitions given}
#Default for seed is 1024 bits
SEED=${3?:1024}
#Runs the PAKE test. Outputs computer stats.
lscpu
for i in $(seq 1 $NUMREPS)
do
  ./pake -p $SEED >> $OUTFILE
done
