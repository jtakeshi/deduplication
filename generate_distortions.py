import sys
import os
from add_noise import noisy
import getopt
import cv2
from wand.image import Image

BASE_PATH = '/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/imagenet_distorted/'

def filename_and_ext(path):
  fname = path.split('/')[-1]
  period_split = fname.split('.')
  return ('.'.join(period_split[:-1]), period_split[-1])

#Strip off the last part when /-split
def dir_name(path):
  split_path = path.split('/')
  ret = '/' + '/'.join(split_path[:-1])
  if ret[-1] != '/':
    ret += '/'
  return ret

def get_hardcoded_params():
  ret = dict()
  ret['blur'] = ["1x0.05", "1x0.1", "1x0.15", "1x0.2", "1x0.25", "1x0.3", "1x0.35", "1x0.4", "1x0.45", "1x0.5"]
  ret['sharpen'] = ["1x0.1", "1x0.2", "1x0.3", "1x0.4", "1x0.5"]
  BRIGHTEN_FACTOR = 0.1
  BRIGHTEN_BASE = 100
  ret['brighten'] = [str(BRIGHTEN_BASE+(BRIGHTEN_FACTOR*x)) for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
  SATURATE_FACTOR = 0.5
  SATURATE_BASE = 100
  ret['saturate'] = [str(SATURATE_BASE+(SATURATE_FACTOR*x)) for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
  SOLARIZE_FACTOR = 0.01
  ret['solarize'] = [str(SOLARIZE_FACTOR*x) for x in [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]]
  GAUSS_FACTOR = 0.01
  ret['gauss'] = [{"mean":0, "variance":s*GAUSS_FACTOR} for s in [1, 2, 3, 4, 5, 6, 7, 8, 9]]
  SP_FACTOR = 0.01
  ret['sp'] = [{"sp":0.5, "amount":str(SP_FACTOR*x)} for x in [0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09]]
  ret['poisson'] = [{'':''}]
  #Generate resize vals
  RESIZE_BASE = 0.99
  RESIZE_UPPERBOUND = 1.01
  RESIZE_STEP = 0.00125
  RESIZE_STEPS = 100
  RESIZE_LIST = list()
  res = RESIZE_BASE
  for i in range(RESIZE_STEPS+1):
    if res > RESIZE_UPPERBOUND:
      break
    RESIZE_LIST.append(res)
    res += RESIZE_STEP
  ret['resize'] = [str(x) for x in RESIZE_LIST if x != 1.0]
  return ret

def output_filename(original_path, dist_type, param, base_path=BASE_PATH):
  split_path = original_path.split('/')
  fname, ext = filename_and_ext(original_path)
  ret = base_path
  if ret[-1] != '/':
    ret += '/'
  return ret + split_path[-2] + '/' + fname + '_' + dist_type + '_' + param + '.' + ext

def blur(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "blur", parameter, base_directory)
  #os.system("convert " + original_filename + " -blur " + parameter + " " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  split_parms = parameter.split('x')
  new_img.blur(sigma=float(split_parms[1]), radius=float(split_parms[0])) #Blur args in hex
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

def sharpen(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "sharpen", parameter, base_directory)
  #os.system("convert " + original_filename + " -sharpen " + parameter + " " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  split_parms = parameter.split('x')
  new_img.sharpen(sigma=float(split_parms[1]), radius=float(split_parms[0])) #Sharpen args in hex
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

#Nothing in Wand for brighten, but modulate will do it
def brighten(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "brighten", parameter, base_directory)
  #os.system("convert " + original_filename + " -modulate " + parameter + ",100 " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  new_img.modulate(brightness=float(parameter))
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

#Nothing in Wand for saturate, but modulate will do it
def saturate(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "saturate", parameter, base_directory)
  #os.system("convert " + original_filename + " -modulate " + "100," + parameter + " " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  new_img.modulate(saturation=float(parameter))
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

def solarize(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "solarize", parameter, base_directory)
  #os.system("convert " + original_filename + " -solarize "  + parameter + "% " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  new_img.solarize(threshold=(1.0-float(parameter))*new_img.quantum_range)
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

def resize(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, "solarize", parameter, base_directory)
  #os.system("convert " + original_filename + " -solarize "  + parameter + "% " + new_name)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_image = Image(filename=original_filename)
  new_img = original_image.clone()
  original_image.close()
  new_img.resize(int(new_img.width * float(parameter)), int(new_img.height * float(parameter)))
  new_img.save(filename=new_name)
  new_img.close()
  return new_name #Return the filename (full path) of the file outputted to

def gaussian(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, 'gauss', str(parameter["variance"]), base_directory)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_img = cv2.imread(original_filename)
  parms = dict()
  for (k, v) in parameter.items():
    parms[k] = float(v)
  noisy_img = noisy('gauss', original_img, parms)
  cv2.imwrite(new_name, noisy_img)
  return new_name

def sp(original_filename, parameter, base_directory):
  new_name = output_filename(original_filename, 'sp', parameter['amount'], base_directory)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_img = cv2.imread(original_filename)
  parms = dict()
  for (k, v) in parameter.items():
    parms[k] = float(v)
  noisy_img = noisy('sp', original_img, parms)
  cv2.imwrite(new_name, noisy_img)
  return new_name

def poisson(original_filename, base_directory):
  new_name = output_filename(original_filename, 'poisson', '', base_directory)
  new_dir = dir_name(new_name)
  if not os.path.isdir(new_dir):
    os.mkdir(new_dir)
  original_img = cv2.imread(original_filename)
  parms = dict()
  noisy_img = noisy('poisson', original_img, None)
  cv2.imwrite(new_name, noisy_img)
  return new_name

def distortion_record_line(original, distortion_type, params, base_directory=BASE_PATH):
  outstr = original + ' ' + distortion_type + ' '
  params_list = list()
  if distortion_type not in ['gauss', 'sp', 'poisson']:
    params_list = params
  else:
    keystr = ''
    if keystr == 'gauss':
      keystr = "variance"
      params_list = [x[keystr] for x in params]
    elif keystr == 'sp':
      keystr = 'amount'
      params_list = [x[keystr] for x in params]
    elif keystr == 'poisson':
      params_list = ['']
  for p in params_list:
    outstr += ' ' + p + ':' + output_filename(original, distortion_type, p, base_directory)
  return outstr


def distort(original_filename, distortion_type, distortion_param, base_directory=BASE_PATH):
  #Do distortion, get a new filename
  #If statements go here to branch by distortion_type
  if distortion_type == 'blur':
    return blur(original_filename, distortion_param, base_directory)
  elif distortion_type == 'sharpen':
    return sharpen(original_filename, distortion_param, base_directory)
  elif distortion_type == 'brighten':
    return brighten(original_filename, distortion_param, base_directory)
  elif distortion_type == 'saturate':
    return saturate(original_filename, distortion_param, base_directory)
  elif distortion_type == 'solarize':
    return solarize(original_filename, distortion_param, base_directory)
  elif distortion_type == "gauss":
    return gaussian(original_filename, distortion_param, base_directory)
  elif distortion_type == "sp":
    return sp(original_filename, distortion_param, base_directory)
  elif distortion_type == "poisson":
    return poisson(original_filename, base_directory)
  elif distortion_type == 'resize':
    return resize(original_filename, distortion_param, base_directory)

def main():
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'd:n:f:o:')
  image_dir_base = '/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/decathlon/imagenet12/train/' #Already has a '/' at the end
  params = None
  distortion_type = None
  num_to_distort = None
  input_filename = ''
  output_directory = ''
  for opt, arg in options:
    if opt in ('-d'):
      distortion_type = arg
    elif opt in ('-n'):
      num_to_distort = int(arg)
    elif opt in ('-f'):
      input_filename = arg
    elif opt in ('-o'):
      output_directory = arg
  #Error checking
  if (distortion_type is None or num_to_distort <= 0) and (input_filename == ''):
    print("ERROR: incorrect parameters, or no parameters given without an input file")
    return

  #Generate subdirectories to search
  '''
  subdirs = os.listdir(image_dir_base)
  image_directories = [image_dir_base + sd + '/' for sd in subdirs]
  '''

  all_params = get_hardcoded_params()
  if distortion_type not in all_params:
    print("ERROR: unrecognized distortion type: " + distortion_type)
    return
  params = all_params[distortion_type]
  infile = open(input_filename,'r')
  images_to_distort = [x.rstrip() for x in infile.readlines()[::2]] #Select every other image to distort
  infile.close()
  '''
  #Choose the first num_to_distort images from each directory
  images_to_distort = list()
  for img_dir in image_directories:
    tmp_images = os.listdir(img_dir)[:num_to_distort]
    for ti in tmp_images:
      images_to_distort.append(img_dir + ti)
  '''

  #f = open(BASE_PATH + distortion_type + '.txt', 'w')
  #Create distortions
  for img in images_to_distort:
    outstr = img + ' ' + distortion_type
    #f.write(img + ' ' + distortion_type)
    for p in params:
      outfile = distort(img, distortion_type, p, output_directory)
      if not os.path.isfile(outfile):
        print("ERROR: file not created: " + outfile)
        return
      if os.stat(outfile).st_size == 0:
        print("ERROR: empty file: " + outfile)
        return  
      parm_str = ''
      if distortion_type == 'gauss':
        parm_str = p["variance"]
      elif distortion_type == 'sp':
        parm_str = p['amount']
      elif distortion_type == 'poisson':
        parm_str = ''
      else:
        parm_str = p
      outstr += ' ' + str(parm_str) + ':' + outfile
      #f.write(' ' + parm_str + ':' + outfile)
    print(outstr)
    #f.write('\n')

  #f.close()

  return

if __name__ == "__main__":
  main()


