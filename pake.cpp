//Jonathan S. Takeshita (jtakeshi@nd.edu)
//pake.cpp
//Test Password-Authenticated Key Exchange
//Compile: g++ pake.cpp -o pake -lgmp -lgmpxx -std=c++11

#include <iostream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <chrono>
//#include <sys/random.h>
#include <random>


#include <getopt.h>
#include <gmp.h>
#include <gmpxx.h>

using namespace std;
using namespace std::chrono;

#define REPS 20

size_t hash_4(const hash<string> & hasher, 
  const mpz_class & XX, const mpz_class & YY,
  const mpz_class & pw, const mpz_class & key);

int main(int argc, char ** argv){
  int prime_bits = -1;
  mpz_class pw_a = -1;
  mpz_class pw_b = -1;
  bool verbose = false;

  //Parse input
  char c;
  while((c = getopt(argc, argv, "a:b:p:v")) != -1){
    switch(c){
      case 'a': {
        pw_a = mpz_class(optarg);
        break;
      }
      case 'b': {
        pw_b = mpz_class(optarg);
        break;
      }
      case 'p': {
        prime_bits = atoi(optarg);
        break;
      }
      case 'v': {
        verbose = true;
        break;
      }
    }
  }

  //Check input
  assert(prime_bits > 0);
  //Get prime number
  mpz_class prime_lower_bound = 1;
  prime_lower_bound = prime_lower_bound << prime_bits;
  mpz_class p;
  mpz_nextprime(p.get_mpz_t(), prime_lower_bound.get_mpz_t());
  assert(mpz_probab_prime_p(p.get_mpz_t(), REPS) && "p must be prime!");
  if(verbose){
    cout << "Lower bound of prime is " << prime_lower_bound << endl;
    cout << "Prime chosen is " << p << endl;
  }
  assert((0 <= pw_a && pw_a < prime_lower_bound) || pw_a == -1);
  assert((0 <= pw_b && pw_b < prime_lower_bound) || pw_b == -1);
  //Init rand. state, based on system randomness
  gmp_randstate_t state;
  gmp_randinit_default(state);
  /*
  unsigned long int seed;
  if(getrandom((void *) & seed, sizeof(seed), 0) != sizeof(seed)){
    cout << "Not enough random entropy available! (initializing seed)" << endl;
    return 0;
  }
  */
  std::random_device rd;
  unsigned int seed = rd();
  gmp_randseed_ui(state, seed);

  //If there was no input for Alice and Bob, initialize it, with a 50/50 chance of the inputs being equal
  if(pw_a == -1){
    assert(pw_b == -1 && "Input must be given for neither or both parties!");
    unsigned int same_pw = rd();
    /*
    if(getrandom((void *) &same_pw, sizeof(same_pw), 0) != sizeof(same_pw)){
      cout << "Not enough random entropy available! (initializing passwords)" << endl;
      return 0;
    }
    */
    mpz_urandomm(pw_a.get_mpz_t(), state, p.get_mpz_t());
    if(same_pw & 1){
      pw_b = pw_a;
    }
    else{
      mpz_urandomm(pw_b.get_mpz_t(), state, p.get_mpz_t());
    }
    if(verbose){
      cout << "Alice's password: " << pw_a << endl;
      cout << "Bob's password: " << pw_b << endl;
    }
  }


  //Get public group element
  mpz_class g;
  mpz_urandomm(g.get_mpz_t(), state, p.get_mpz_t());

  //Get public elements for Alice and Bob
  hash<string> hasher;
  mpz_class M_a = hasher("Alice");
  mpz_class M_b = hasher("Bob");

  //Alice is the preprocessing party, Bob is the party whom we are timing

  //Choose Alice's random number
  mpz_class x;
  mpz_urandomm(x.get_mpz_t(), state, p.get_mpz_t());
  if(verbose){
    cout << "Alice's random element x: " << x << endl;
  }
  //Calculate exponent and message
  mpz_class X = x*g;
  X = X % p;
  mpz_class XX = X+(M_a*pw_a);
  XX = XX % p;

  //Start Bob's computation here
  auto bob_start = high_resolution_clock::now();
  //Choose Bob's random number
  mpz_class y;
  mpz_urandomm(y.get_mpz_t(), state, p.get_mpz_t());
  //Calculate exponent and message
  mpz_class Y = y*g;
  Y = Y % p;
  mpz_class YY = Y+(M_b*pw_b);
  YY = YY % p;
  //Compute shared key
  mpz_class K_B = (XX-(M_a*pw_b))*y;
  K_B = K_B % p;
  size_t SK_B = hash_4(hasher, XX, YY, pw_b, K_B);
  //Bob is now finished
  auto bob_duration = high_resolution_clock::now() - bob_start;
  //Print Bob's random number AFTER computation finishes
  if(verbose){
    cout << "Bob's random element y: " << y << endl;
  }

  //Do Alice's computation
  //Compute shared key
  mpz_class K_A = (YY-(M_b*pw_a))*x;
  K_A = K_A % p;
  size_t SK_A = hash_4(hasher, XX, YY, pw_a, K_A);

  //Test!
  bool equal = true;
  if(pw_a == pw_b){
    assert(SK_A == SK_B);
    if(verbose){
      cout << "Passwords " << pw_a << " and " << pw_b << " were equal" << endl;
      cout << "Keys are " << SK_A << " and " << SK_B << endl;
    }
  }
  else{
    equal = false;
    assert(SK_A != SK_B);
    if(verbose){
      cout << "Passwords " << pw_a << " and " << pw_b << " were not equal" << endl;
      cout << "Keys are " << SK_A << " and " << SK_B << endl;
    }
  }

  //Output how long it took
  auto time_microseconds = duration_cast<microseconds>(bob_duration);
  if(verbose){
    cout << "Time for Bob to compute: "
    << (long long) time_microseconds.count() << 
    " microseconds" << std::endl;
    cout << "Equal passwords: " << equal << endl;
  }
  else{
    //Output time, prime bits and equality check in .csv format
    cout << (long long) time_microseconds.count() << ',' << prime_bits << ',' << equal << endl;
  }
  
  return 0;
}


size_t hash_4(const hash<string> & hasher, 
  const mpz_class & XX, const mpz_class & YY,
  const mpz_class & pw, const mpz_class & key){
  string s = "";
  s += XX.get_str();
  s += YY.get_str();
  s += pw.get_str();
  s += key.get_str();
  return hasher(s);
}
