from LLSH import LLSH
from list_features import get_model
from list_features import image_features
import time
import sys
import getopt



#Returns a list of pairs of (up to n) filenames and collisions 
def similar_files(featureVec, hashObj, n=None, isSparse=False):
  #sim_time = time.time()
  #candidates = hashObj.query(featureVec, n)
  #ret = [x[0] for x in candidates]
  #if isSparse:
    #print("Shape: " + ret.shape())
    #ret = ret[0][0]
  #numsimilars = len(ret)
  #print("#Time to calculate similarity for " + str(numsimilars) + " files: " + str(1000*(time.time() - sim_time)))
  #return ret
  return hashObj.query(featureVec, n)

def main():
  NUM_FEATURES = 2048 #Size of the feature vector - 4096 for VGG, 2048 for ResNet
  hash_size = 7 #May have to change this - I guessed at computer word size, then powers of 2
  num_tables = 8
  num_results = 3 
  #metric = 'euclidean' #If an explicit option is given, the SparseLSH library crashes
  metric = None
  infile= ''    
  matrix_filename = ''
  shelvefile = ''
  isSparse=True
  persistence_type = 'dbm'
  num_errors = 1
  query_filename = ''
  num_queries=0
  remove_substrings = True

  #PLSH arguments
  #Use PLSH by default
  private = True
  salt = None
  num_bytes = 32
  ratio = 0

  #CHANGE THIS LINE UPON CHANGING ARGUMENTS
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'h:t:s:m:a:b:p:e:f:n:y')

  for opt, arg in options:
    if opt in ('-h'):
      hash_size = int(arg)
      #print("hash_size " + str(hash_size)) 
    elif opt in ('-t'):
      num_tables = int(arg)  
    elif opt in ('-m'):
      matrix_filename = arg
    elif opt in ('-s'):
      shelvefile = arg
    elif opt in ('-a'):
      salt = arg  
    elif opt in ('-b'):
      num_bytes = int(arg)
    elif opt in ('-p'):
      private = False if arg in ('false', 'False', 'FALSE', '0', 'no-privacy') else True       
    elif opt in ('-e'):
      num_errors = int(arg)  
    elif opt in ('-f'):
      query_filename = arg
    elif opt in ('-n'):
      num_queries = int(arg)  
    elif opt in ('-y'):
      remove_substrings = True if arg not in ('false', 'False', 'FALSE', '0', 'no-privacy') else False       

  #Check inputs
  bad_input = False
  if matrix_filename == '':
  	print("No matrices file given!")
  	bad_input = True
  if shelvefile == '':	
  	print("No database given!")
  	bad_input = True    

  if bad_input:
  	return
  	
  #Decide whether to use CLI filenames or file-based filenames
  #One or the other for now - could allow both
  input_list = None
  if query_filename == '':
    input_list = remainders
  else:
    infile = open(query_filename, "r") 
    input_list = infile.read().splitlines()
    infile.close()
  input_list = [fn.rstrip() for fn in input_list if fn[0] != '#']
  #Limit the number of queries
  if num_queries > 0:
    input_list = input_list[:num_queries]
  

  #Construct PLSH args
  privacy_config = None
  if private:
    privacy_config = dict()
    privacy_config['salt'] = salt
    privacy_config['bytes'] = num_bytes	

  #Number of nonequal hashes tolerated
  print("#Require " + str(num_tables-num_errors) + " collisions for similarity")

  storage_config = dict()
  #TODO update naming
  storage_config[persistence_type] = shelvefile    

  hashObj = LLSH(hash_size, NUM_FEATURES, num_tables, storage_config, matrix_filename, overwrite=False, private_config=privacy_config)
  model = get_model()
  feature_vec = dict()
  #Extract features here

  for filename in input_list:
    #Ignore comments
    if filename[0] == '#':
      continue
    feature_vec[filename] = image_features(filename, model)
  num_queried = 0
  search_start = time.time()
  for filename in input_list:
    #Ignore comments
    if filename[0] == '#':
      continue
    num_queried = num_queried + 1  
    cand_files = similar_files(feature_vec[filename], hashObj)
  	#Pare down candidates to nonequal filenames and closely related files
  	#DEBUG
    print_all_files = False
    similars = [f for f in cand_files if ((not remove_substrings) or filename not in f[0]) and (f[1] >= num_tables-num_errors)]
    if len(similars) > 0 or print_all_files:
      print("Files similar to " + filename + ":")
      for s in similars:
        print("\t" + s[0] + "\t" + str(s[1]))
  	#else:
  		#print("No similar files for " + filename)		
  print("#Queried against " + shelvefile)
  print("#Time to query " + str(num_queried) + " images: " + str((time.time()-search_start)*1000) + " ms")
  #DEBUG
  #print(hashObj)
  


if __name__ == "__main__":
  main()  
