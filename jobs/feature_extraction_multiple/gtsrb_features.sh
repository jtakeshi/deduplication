#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N multiple_FE_gtsrb
#$ -t 1-9

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python list_features.py ./filenames/gtsrb_filenames.txt > ./features/timetrials.$SGE_TASK_ID/gtsrb_features.txt
deactivate
