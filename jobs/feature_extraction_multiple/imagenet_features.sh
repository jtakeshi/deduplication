#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N multiple_FE_imagenet
#$ -t 1-4


source ./virt-dedup/bin/activate
module load tensorflow
#Store 
#Relies on my symlink
time ./virt-dedup/bin/python list_features.py ./filenames/imagenet_filenames.txt > ~/dsp-lab/vol1/deduplication/features/imagenet_features_${SGE_TASK_ID}.txt
tail -n 1 ~/dsp-lab/vol1/deduplication/features/imagenet_features_${$SGE_TASK_ID}.txt > ~/Documents/deduplication/features/timetrials.${SGE_TASK_ID}/imagenet_time.txt
deactivate
