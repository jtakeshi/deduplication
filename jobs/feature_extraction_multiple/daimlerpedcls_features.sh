#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N multiple_FE_daimler
#$ -t 1-10

DATASET=daimlerpedcls

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python list_features.py ./filenames/${DATASET}_filenames.txt > ~/dsp-lab/vol1/deduplication/features/${DATASET}_features_${SGE_TASK_ID}.txt
tail -n 1 ~/dsp-lab/vol1/deduplication/features/${DATASET}_features_${SGE_TASK_ID}.txt > ~/Documents/deduplication/features/timetrials.${SGE_TASK_ID}/${DATASET}_time.txt
deactivate
