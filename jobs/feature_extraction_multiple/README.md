These scripts run feature extraction 9 times (except for imagenet, which only gets run 4 times), for a total of 10 (5) trials per dataset.
Assumes run from root repo dir., and existence of directories ./features/timetrials.$x/ for $x in [1,9]
