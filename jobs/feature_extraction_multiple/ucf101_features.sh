#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N multiple_FE_ucf101
#$ -t 1-9

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python list_features.py ./filenames/ucf101_filenames.txt > ./features/timetrials.$SGE_TASK_ID/ucf101_features.txt
deactivate
