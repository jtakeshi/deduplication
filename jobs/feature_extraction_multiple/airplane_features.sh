#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N multiple_FE_aircraft
#$ -t 1-9

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python list_features.py ./filenames/aircraft_filenames.txt > ./features/timetrials.$SGE_TASK_ID/aircraft_features.txt
deactivate
