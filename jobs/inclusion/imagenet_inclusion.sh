#!/bin/bash

#$ -q *@@jung
#$ -N inclusion_imagenet
#$ -pe smp 24
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
DATANUM=1
QUERIES=100

RESULT_DIR=incl_results
echo "Using $QUERIES queries"
source ./virt-dedup/bin/activate
time ./virt-dedup/bin/python inclusion_query.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -n $QUERIES -f ./features/features/${DATASET}_features.txt > ./${RESULT_DIR}/${DATASET}.csv
deactivate
