#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N hash_timing

DATASET=cifar100
HASHSIZE=24
TABLES=6
NUMTRIALS=4000

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python time_hash.py -h ${HASHSIZE} -t ${TABLES} -m ./databases/timing_matrices.npz -s ./databases/timing_db -n ${NUMTRIALS} ./features/features/${DATASET}_features.txt > ./hash_results/hash_time_results.csv
deactivate
