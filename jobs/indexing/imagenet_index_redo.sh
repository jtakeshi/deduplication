#!/bin/bash

#$ -q *@@jung
#$ -N imagenet_idx_redo
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
HASHSIZE=24
TABLES=6
STORAGE_LOCATION=~/dsp-lab/vol1/deduplication

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python build_index.py -h $HASHSIZE -t $TABLES -m ${STORAGE_LOCATION}/databases/${DATASET}_matrices.npz -s ${STORAGE_LOCATION}/databases/${DATASET}_index ./features/features/${DATASET}_features.txt > ./index_results/imagenet_redo_results.txt
deactivate
