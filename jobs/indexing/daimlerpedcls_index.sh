#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N daimlerpedcls_idx
#$ -t 1-10

DATASET=daimlerpedcls
HASHSIZE=24
TABLES=6
STORAGE_LOCATION=~/dsp-lab/vol1/deduplication

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python build_index.py -h $HASHSIZE -t $TABLES -m ${STORAGE_LOCATION}/databases/${DATASET}_${SGE_TASK_ID}_matrices.npz -s ${STORAGE_LOCATION}/databases/${DATASET}_${SGE_TASK_ID}_index ./features/features/${DATASET}_features.txt > ./index_results/$SGE_TASK_ID/${DATASET}_results.txt
deactivate
