#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N imagenet_fromdir
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
HASHSIZE=24
TABLES=6
STORAGE_LOCATION=~/dsp-lab/vol1/deduplication
TARGET_DIR=/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/decathlon/imagenet12/train
NUM_FROM_DIR=10

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python index_from_basedir.py -h $HASHSIZE -t $TABLES -m ${STORAGE_LOCATION}/databases/${DATASET}_fromdir_matrices.npz -s ${STORAGE_LOCATION}/databases/${DATASET}_fromdir_index $TARGET_DIR -n $NUM_FROM_DIR > ./index_results/imagenet_fromdir.txt
deactivate
