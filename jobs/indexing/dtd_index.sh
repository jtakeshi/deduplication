#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N dtd_index
#$ -t 1-10

DATASET=dtd
HASHSIZE=24
TABLES=6

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python build_index.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${SGE_TASK_ID}_matrices.npz -s ./databases/${DATASET}_${SGE_TASK_ID}_index ./features/features/${DATASET}_features.txt > ./index_results/$SGE_TASK_ID/${DATASET}_results.txt
deactivate
