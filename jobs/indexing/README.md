Tests right now are being run with a hash size of 24 bits, as a moderate option between 32 (more than large enough, but probably slower), and 16 (too small, false positive collisions likely).
We use 6 tables for a similar reason.
Assumes existence of directories ./index_results/$x for x in [1,10], except for imagenet, which has x in [1,5]
