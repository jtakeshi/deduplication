#!/bin/bash

#$ -q gpu@@jung_gpu
#$ -l gpu=1
#$ -pe smp 1
#$ -N dist_query_fromfile
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
#DATANUM=1
STORAGE_LOCATION=~/dsp-lab/vol1/deduplication
OUTDIR="/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/file_distortions/"
#Set to 0 to query all lines
NUM_LINES_TO_QUERY=0


#declare -a distortions=("blur" "brighten" "gauss" "resize" "saturate" "sharpen" "solarize" "sp")
#Just redoing these
declare -a distortions=("sp" "solarize" "gauss")
#DO NOT ARGUE -o to distortion_query.py
source ./virt-dedup/bin/activate
module load tensorflow
for i in "${distortions[@]}"
do
	echo $i
	time ./virt-dedup/bin/python distortion_query.py -h $HASHSIZE -t $TABLES -m ${STORAGE_LOCATION}/databases/${DATASET}_fromdir_matrices.npz -s ${STORAGE_LOCATION}/databases/${DATASET}_fromdir_index -f ${OUTDIR}${i}.txt -d $i -n $NUM_LINES_TO_QUERY > ./distortion_results/${DATASET}/${i}_fromfile.txt
done
deactivate
