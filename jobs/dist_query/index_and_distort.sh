#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu=1
#$ -pe smp 1
#$ -N ind_and_dist
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
#DATANUM=1
BASEDIR="/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/decathlon/imagenet12/train/"
NUM_TO_DISTORT=5

declare -a distortions=("blur" "brighten" "gaussian" "resize" "saturate" "sharpen" "solarize" "sp")

#Make sure the database files used are unique
source ./virt-dedup/bin/activate
module load tensorflow
for i in "${distortions[@]}"
	OVERWRITE=""
	if [$i == "blur"]
	then
		OVERWRITE='-o'
	fi
	echo $i
	echo $OVERWRITE
	time ./virt-dedup/bin/python index_and_distort.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_iad_matrices.npz -s ./databases/${DATASET}_iad_index $OVERWRITE -n $NUM_TO_DISTORT -d $i $BASEDIR > ./distortion_results/iad_${DATASET}/${i}.txt

deactivate


