#!/bin/bash

#$ -q gpu@@jung_gpu
#$ -l gpu=1
#$ -pe smp 1
#$ -N dist_onthefly
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
#DATANUM=1

declare -a distortions=("blur" "brighten" "gaussian" "resize" "saturate" "sharpen" "solarize" "sp")
#Make sure the database files used are unique
source ./virt-dedup/bin/activate
module load tensorflow
for i in "${distortions[@]}"
do
	echo $i
	time ./virt-dedup/bin/python distortion_query.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_fly_matrices.npz -s ./databases/${DATASET}_fly_index -f ./images/${DATASET}_distorted/${i}.txt -d $i -o > ./distortion_results/fly_${DATASET}/${i}.txt
done


deactivate
