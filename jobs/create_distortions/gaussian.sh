#!/bin/bash

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N dist_gaussian
#$ -M jtakeshi@nd.edu
#$ -m abe

IMAGES_FROM_EACH_FOLDER=10
DISTORTION=gaussian

source ./virt-dedup/bin/activate
time ./virt-dedup/bin/python generate_distortions.py -d $DISTORTION -n $IMAGES_FROM_EACH_FOLDER
deactivate
