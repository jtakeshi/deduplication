#!/bin/bash

#$ -q gpu@@jung_gpu
#$ -l gpu=1
#$ -pe smp 24
#$ -N dist_from_file
#$ -M jtakeshi@nd.edu
#$ -m ae
#Change this whenever the distortions are changed
#$ -t 1-3

DATASET=imagenet
#Don't change these without first rebuilding the index
#HASHSIZE=24
#TABLES=6
#We use indices from trial 1
#DATANUM=1
#BASEDIR="/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/decathlon/imagenet12/train/"
OUTDIR="/afs/crc.nd.edu/user/j/jtakeshi/Documents/deduplication/images/file_distortions/"
NUM_TO_DISTORT=20

#declare -a distortions=("blur" "brighten" "gauss" "resize" "saturate" "sharpen" "solarize" "sp")
#Only need to redo these few
declare -a distortions=("sp" "solarize" "gauss")
let dist_idx=$SGE_TASK_ID-1
DISTORTION="${distortions[$dist_idx]}"

#Make sure the database files used are unique
source ./virt-dedup/bin/activate
module load tensorflow
echo "${DISTORTION} ..."
time ./virt-dedup/bin/python generate_distortions.py -d $DISTORTION -f ./index_results/${DATASET}_fromdir.txt -o $OUTDIR > ${OUTDIR}${DISTORTION}.txt
echo "${DISTORTION} complete"

#for i in "${distortions[@]}"
#do
#	echo $i
#	time ./virt-dedup/bin/python generate_distortions.py -d $i -f ./index_results/${DATASET}_fromdir.txt -o $OUTDIR > $OUTDIR/${i}.txt
#done
deactivate


