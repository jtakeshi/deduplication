#!/bin/bash

#$ -q *@@jung
#$ -N qos_imagenet
#$ -pe smp 24
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
DATANUM=1
QUERIES=100


#num_threads=("1" "2" "4" "8" "16" "32" "64" "128" "256" "512" "1024" "2048" "4096" "8192" "16384" "32678" "65536" "131072" "162144" "524288" "1048567")
num_threads="1:2:4:8:16:32:64:256:512:1024:2048:4096:8192:16384:32678:65536:131072:262144:524288:1048567"
#Just for testing
#num_threads=(1)

#Clear previous results
rm ./qos_results/${DATASET}.csv

source ./virt-dedup/bin/activate
#module load tensorflow
#time ./virt-dedup/bin/python query.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -f ./queries/${DATASET}_query.txt -n $QUERIES > ./query_results/$SGE_TASK_ID/${DATASET}_${SGE_TASK_ID}_constresults.txt
#for i in "${num_threads[@]}"
#do
#  ./virt-dedup/bin/python qos.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -n $i -f ./features/features/${DATASET}_features.txt >> ./qos_results/${DATASET}.csv
#done
./virt-dedup/bin/python qos.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -n $num_threads -f ./features/features/${DATASET}_features.txt >> ./qos_results/${DATASET}.csv

deactivate
