#!/bin/bash

#$ -q *@@tjung
#TODO use a parallel environment
#$ -N qos_aircraft

DATASET=aircraft
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
DATANUM=1
QUERIES=100


#num_threads=(1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048)
#Just for testing
num_threads=(1)

source ./virt-dedup/bin/activate
module load tensorflow
#time ./virt-dedup/bin/python query.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -f ./queries/${DATASET}_query.txt -n $QUERIES > ./query_results/$SGE_TASK_ID/${DATASET}_${SGE_TASK_ID}_constresults.txt
for i in "${num_threads[@]}"
do
  ./virt-dedup/bin/python qos.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -n $i -f ./features/features/${DATASET}_features.txt
done
deactivate
