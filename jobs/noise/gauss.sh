#!/bin/bash
#Call from repo root
#Arguments - variance mean image
IMAGE=$3
FNAMEPATH="${IMAGE%.*}"
EXT="${IMAGE##*.}"

source ./virt-dedup/bin/activate
#echo $3
#echo $FNAMEPATH
#echo $EXT
echo ${FNAMEPATH}_${1}_${2}.${EXT}
which python
./virt-dedup/bin/python add_noise.py -n gauss -v $1 -m $2 $3 -o ${FNAMEPATH}_${1}_${2}.${EXT}
deactivate
