#!/bin/bash0

#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N q_ucf101
#$ -t 1-10

DATASET=ucf101
#Don't change these without first rebuilding the index
HASHSIZE=24
TABLES=6
#We use indices from trial 1
DATANUM=1
QUERIES=100

source ./virt-dedup/bin/activate
module load tensorflow
time ./virt-dedup/bin/python query.py -h $HASHSIZE -t $TABLES -m ./databases/${DATASET}_${DATANUM}_matrices.npz -s ./databases/${DATASET}_${DATANUM}_index -f ./queries/${DATASET}_query.txt -n $QUERIES > ./query_results/$SGE_TASK_ID/${DATASET}_${SGE_TASK_ID}_constresults.txt
deactivate
