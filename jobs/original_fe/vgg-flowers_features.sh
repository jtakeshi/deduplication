#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@crc_1080ti
#$ -l gpu_card=1
#$ -N feature_extraction_vgg-flowers

echo "GPU information:"
lspci -vnn | grep VGA -A 12
echo "CPU information:"
lscpu
source ./tf-gpu/bin/activate
#echo "Loaded env."
module load tensorflow
#echo "Loaded module"
#echo "pip is:"
#which pip
#echo "Packages are:"
#~/Documents/deduplication/virt-dedup/bin/pip freeze
time ./virt-dedup/bin/python list_features.py ./filenames/vgg-flowers_filenames.txt > ./features/vgg-flowers_features.txt
deactivate
