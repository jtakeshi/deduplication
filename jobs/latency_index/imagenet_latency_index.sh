#!/bin/bash

#$ -q *@@jung
#$ -N lat_imagenet
#$ -pe smp 24
#$ -M jtakeshi@nd.edu
#$ -m abe

DATASET=imagenet
HASHSIZE=24
TABLES=6
STORAGE_LOCATION=~/dsp-lab/vol1/deduplication
num_threads="1:2:4:8:16:32:64:256:512:1024:2048:4096:8192:16384:32678:65536:131072:262144:524288:1048567"


source ./virt-dedup/bin/activate
#Clear previous results
rm ./latency_index_results/${DATASET}_results.csv
time ./virt-dedup/bin/python latency_index.py -h $HASHSIZE -t $TABLES -m ${STORAGE_LOCATION}/tmp_files/${DATASET}_matrices.npz -s ${STORAGE_LOCATION}/tmp_files/${DATASET}_index -n $num_threads ./features/features/${DATASET}_features.txt > ./latency_index_results/${DATASET}_results.csv
deactivate
