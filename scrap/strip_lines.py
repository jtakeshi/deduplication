from __future__ import print_function
import sys


def main():

  helpstr = "Exactly two ordered arguments should be given: filename ratio"
  if len(sys.argv) != 3:
    print(helpstr)
    sys.exit(0)
  filename = sys.argv[1]
  ratio = int(sys.argv[2])
  fileobj = open(filename, "rt")
  #This could be made more efficient by not stripping the line here and setting end='' in the print - the current way is more clear
  stripped_lines = [val.rstrip() for i, val in enumerate(fileobj) if i % ratio == 0]
  for line in stripped_lines:
    print(line)



if __name__ == "__main__":
  main() 
