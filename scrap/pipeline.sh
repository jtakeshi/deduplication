#!/bin/sh
python list_features.py images/101_ObjectCategories/*/*.jpg > features/101_ObjectCategories.txt
python build_index.py -h 16 -t 4 -m databases/matrices.npz -s databases/101_ObjectCategories.txt features/101_ObjectCategories.txt > 101_full.txt
python query.py -h 16 -t 4 -m databases/matrices.npz -s databases/101_ObjectCategories.txt images/101_ObjectCategories/*/image_000*.jpg >> 101_full.txt
